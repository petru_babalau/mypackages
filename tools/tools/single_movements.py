import wx
import action
from utils import Header, Row
from utils import Field


LOADING_MANIP = '00000080'
MANIPULATOR_ROT = '00000080'
MANIPULATOR_POS = '00000080'
FOLDING_BEAM = '00000080'
FOLDING_BEAM_HEIGHT = '00000080'
CLOSE_OPEN_MAIN_MANIP = ('20', '4b')
CLOSE_OPEN_LOAD_MANIP_CLAMPS = ('20', '4c')


hex_string = '06000000000000000000' \
             '13000000000000000000' \
             + FOLDING_BEAM +\
             '00000080 00000080 00000080' \
             + FOLDING_BEAM_HEIGHT +\
             '00000080 00000080 00000080' \
             + MANIPULATOR_ROT +\
             + MANIPULATOR_POS +\
             '00000080' \
             + LOADING_MANIP +\
             '00000080' \
             '20202020 20202020 2020' \
             + CLOSE_OPEN_MAIN_MANIP[0] +\
             + CLOSE_OPEN_LOAD_MANIP_CLAMPS[0] +\
             '20202020 20202020 20202020 20202020 20202020 2020 ' \
             '00000080 00000080 00000080 00000080' \
             '20202020 20202020'