# -*- coding: utf-8 -*-
import wx
import action
from utils import Header, Row
from utils import Field


#-----------------------------------------------------------------------------
# UPPERBEAM CLAMP
POSITION_CLAMP = ('00000080', 'xx000000')
REDUCED_CLAMP_SPEED = ('20', '42')
NO_WAIT = ('20', '4d')
CORNER_PIECES_UPSTROKE = ('20', '4b')

# UPPERBEAM OPEN
POSITION_OPEN = ('00000080', 'xx000000')
NO_WAIT = ('20', '4d')
CORNER_PIECES_SWING_UPSTROKE = ('20', '4C')
CORNER_PIECES_UPSTROKE = ('20', '49', '4C')

# UPPERBEAM HEMMING
PULL_BACK = ('00000080', 'xx000000')
UPPER_BEAM_DOWN = ('00000080', 'xx000000')
PRESSURE = ('00000080', 'xx000000')
UPPER_BEAM_UP = ('00000080', 'xx000000')
REDUCED_CLAMP_SPEED = ('20', '42')
#-----------------------------------------------------------------------------


###############################################################################
# Actions
###############################################################################
class Actions:
    clamp = Field(key='clamp', label='CLAMP')
    ub_open = Field(key='open', label='OPEN')
    hemming = Field(key='hemming', label='HEMMING')
#------------------------------------------------------------------------------


HEX_STR = {
    Actions.clamp.key: '03' + '00' * 9 +
                       '08' + '00' * 9 +
                       '00000080' +
                       '{position_clamp}' +
                       '0000008000000080000000800000008000000080' +
                       '00000080000000800000008000000080' +
                       '000000800000008020' +
                       '{reduced_clamp_speed}' +
                       '2020202020202020' +
                       '{corner_pieces_upstroke}' +
                       '20' +
                       '{no_wait}' +
                       '202020202020202020202020202020202020202020' +
                       '00000080000000800000008000000080' +
                       '20' * 8,
    Actions.ub_open.key: '03' + '00' * 9 +
                         '09' + '00' * 9 +
                         '00000080' * 2 +
                         '{position_open}' +
                         '00000080' * 10 +
                         '20' * 8 +
                         '{corner_pieces_upstroke}' +
                         '2020' +
                         '{corner_pieces_swing_upstroke}' +
                         '{no_wait}' +
                         '20' * 21 +
                         '00000080' * 4 +
                         '20' * 8,
    Actions.hemming.key: '03' + '00' * 9 +
                         '0a' + '00' * 9 +
                         '00000080' +
                         '{upper_beam_down}' +
                         '{upper_beam_up}' +
                         '00000080' * 6 +
                         '{pull_back}' +
                         '00000080' * 3 +
                         '20' +
                         '{reduced_clamp_speed}' +
                         '20' * 32 +
                         '00000080' * 2 +
                         '{pressure}' +
                         '00000080' +
                         '20' * 8
}


###############################################################################
# Variables
###############################################################################
class Variables:
    position_clamp = Field(key='position_clamp', label='POSITION CLAMP')
    position_open = Field(key='position_open', label='POSITION OPEN')
    pull_back = Field(key='pull_back', label='PULL BACK')
    ub_down = Field(key='ub_down', label='UB DOWN')
    pressure = Field(key='pressure', label='PRESSURE')
    ub_up = Field(key='ub_up', label='UB UP')
    reduced_clamp_speed = Field(key='reduced_clamp_speed', label='REDUCED_CLAMP_SPEED')
    no_wait = Field(key='no_wait', label='NO_WAIT')
    corner_pieces_upstroke = Field(key='corner_pieces_upstroke',
                                            label='CORNER_PIECES_UPSTROKE')
    position_open = Field(key='position_open', label='POSITION_OPEN')
    corner_pieces_swing_upstroke = Field(key='corner_pieces_swing_upstroke',
                                            label='CORNER_PIECES_SWING_UPSTROKE')
    upper_beam_down = Field(key='upper_beam_down', label='UPPER_BEAM_DOWN')
    upper_beam_up = Field(key='upper_beam_up', label='UPPER_BEAM_UP')
#------------------------------------------------------------------------------


FIELDS = {
    Actions.clamp.key: [Variables.position_clamp.key,
                        Variables.reduced_clamp_speed.key,
                        Variables.no_wait.key,
                        Variables.corner_pieces_upstroke.key],
    Actions.ub_open.key: [Variables.position_open.key,
                            Variables.no_wait.key,
                            # Variables.corner_pieces_swing_upstroke.key,
                            Variables.corner_pieces_upstroke.key],
    Actions.hemming.key: [Variables.pull_back.key,
                            Variables.ub_down.key,
                            Variables.pressure.key,
                            Variables.ub_up.key,
                            Variables.upper_beam_down.key,
                            Variables.upper_beam_up.key,
                            Variables.reduced_clamp_speed.key]
}


DEFAULT_VALUES = {
    Actions.clamp.key: {Variables.position_clamp.key: '0.0',
                        Variables.reduced_clamp_speed.key: 'DEACTIVATED',
                        Variables.no_wait.key: 'DEACTIVATED',
                        Variables.corner_pieces_upstroke.key: 'DEACTIVATED'},
    Actions.ub_open.key: {Variables.position_open.key: '0.0',
                            Variables.no_wait.key: 'DEACTIVATED',
                            # Variables.corner_pieces_swing_upstroke.key: 'DEACTIVATED',
                            Variables.corner_pieces_upstroke.key: ''},
    Actions.hemming.key: {Variables.pull_back.key: '0.0',
                            Variables.ub_down.key: '0.0',
                            Variables.pressure.key: '0.0',
                            Variables.ub_up.key: '0.0',
                            Variables.upper_beam_down.key: '0.0',
                            Variables.upper_beam_up.key: '0.0',
                            Variables.reduced_clamp_speed.key: 'DEACTIVATED'}
}


OPTIONS = {
    Actions.clamp.key: {'reduced_clamp_speed': False,
                        'no_wait': False,
                        'corner_pieces_working_position': False},
    Actions.ub_open.key: {'no_wait': False,
                            'corner_pieces_swing_upstroke': False,
                            'corner_pieces_upstroke': False},
    Actions.hemming.key: {'reduced_clamp_speed': False}
}


# TODO:
HEX_VALUES = {}


###############################################################################
# UpperBeamModel
###############################################################################
class UpperBeamModel:
    actions = [Actions.clamp.key,
               Actions.ub_open.key,
               Actions.hemming.key]

    def __init__(self, *args, **kwargs):
        action = kwargs.get('action', self.actions[0])
        self.hex_str = ''
        self._init_action(action)

    def __str__(self):
        string = '<UpperBeam>'
        for attr in self.__dict__:
            string += '\n'
            string += '\t' + attr + ' = ' + str(getattr(self, attr))
        string += '\n</UpperBeam>'
        return string

    def formated_hex_str(self):
        """ Returns a formated hex string. """
        data = {}
        for attr in FIELDS[self.action]:
            data[attr] = HEX_VALUES[attr]
        formated_hex_str = self.hex_str.format(**data)
        return formated_hex_str

    def _init_action(self, action):
        self.action = action
        self.hex_str = HEX_STR[action]
        for item in DEFAULT_VALUES[action].items():
            setattr(self, item[0], item[1])

    def change_action(self, action=None):
        self.delete_previous_attributes()
        self.reset_hex_str(action)
        self.action = action
        self._init_action(action)
        print(self)

    def reset_hex_str(self, action):
        self.hex_str = HEX_STR[action]

    def delete_previous_attributes(self):
        for attr in FIELDS[self.action]:
            delattr(self, attr)
#------------------------------------------------------------------------------


###############################################################################
# Upper beam: Basic functions
#
# 0: Upper beam down (clamping)
# 1: Upper beam up
# 2: Hemming (to dimension; closed hem)
#
###############################################################################

CLAMP = 0
OPEN = 1
HEMMING = 2

BASIC_FUNC_LABELS = {
    0: 'CLAMP',
    1: 'OPEN',
    2: 'HEMMING'
}
INV_BASIC_FUNC_LABELS = {v: k for k, v in BASIC_FUNC_LABELS.items()}


class UpperBeam(wx.Panel, action.Action):

    def __init__(self, parent=None, basic_func=CLAMP):
        super(UpperBeam, self).__init__(parent)
        self.id = id(self)
        self._action = 'clamp'
        self.model = UpperBeamModel(action=self._action)
        self.header = {'label': "UPPER-BEAM",
                        'value': BASIC_FUNC_LABELS[basic_func],
                        'type': 'ComboBox',
                        'choices': list(BASIC_FUNC_LABELS.values()),
                        'tooltip': 'CLAMP: Close the upper beam.\n\n'
                                   'OPEN: Opens the upper beam.\n\n'
                                   'HEMMING: Creates a hem.'
        }
        self.params = []
        self.options = []
        self.rows = []
        self.bgcolor = wx.Colour(146, 153, 161)
        self.SetBackgroundColour(self.bgcolor)
        self.SetDoubleBuffered(True)
        self.__do_layout()
        self.__do_binds()

        if basic_func == CLAMP:
            self.clamp()
        elif basic_func == OPEN:
            self.open()
        elif basic_func == HEMMING:
            self.hemming()

    def __do_binds(self):
        if self.header['type'] == 'ComboBox':
            getattr(self.header_obj, 'value').Bind(wx.EVT_COMBOBOX, self.__handle_combobox)

    def __handle_combobox(self, event):
        basic_func = getattr(self.header_obj, 'value').GetValue()
        if INV_BASIC_FUNC_LABELS[basic_func] == CLAMP:
            self.clamp()
        elif INV_BASIC_FUNC_LABELS[basic_func] == OPEN:
            self.open()
        elif INV_BASIC_FUNC_LABELS[basic_func] == HEMMING:
            self.hemming()
        event.Skip()

    def clamp(self):
        """ Set CLAMP parameters and options and update layout. """
        self._action = Actions.clamp.key
        self.model.change_action(self._action)
        self.params = [
                        {'label': 'POSITION CLAMP',
                            'type': 'TextCtrl',
                            'value': DEFAULT_VALUES[self._action]['position_clamp'],
                            'mu': 'mm',
                            'key': 'position_clamp',
                            'mode': 'variable',
                            'tooltip': 'E.g. Material thickness minus "clamping pressure" dimension.\n\n'
                                       '"Clamping pressure" dimension:\n'
                                       '   0.2mm\tfor normal materials\n'
                                       '   0.1mm\t for sensitive, thin materials'
                         }
                      ]
        self.options = [
                        {'label': 'REDUCED_CLAMP_SPEED',
                            'type': 'ComboBox',
                            'value': DEFAULT_VALUES[self._action]['reduced_clamp_speed'],
                            'mu': '',
                            'choices': ['ACTIVATED', 'DEACTIVATED'],
                            'key': 'reduced_clamp_speed',
                            'mode': 'option',
                            'tooltip': 'Reduced upper beam speed (in %).\n'
                                       'Determined by a parameter (see 8.10.1)'},
                        {'label': 'NO_WAIT',
                            'type': 'ComboBox',
                            'value': DEFAULT_VALUES[self._action]['no_wait'],
                            'mu': '',
                            'choices': ['ACTIVATED', 'DEACTIVATED'],
                            'key': 'no_wait',
                            'mode': 'option',
                            'tooltip': 'Starts the actual program line.\n'
                                       'The following line will be started also.\n\n'
                                       'Important: Make sure, that you know about the consequences when you use '
                                       'the No. WAIT-function.'},
                        {'label': 'CORNER_PIECES_UPSTROKE',
                            'type': 'ComboBox',
                            'value': DEFAULT_VALUES[self._action]
                                                    ['corner_pieces_upstroke'],
                            'mu': '',
                            'choices': ['ACTIVATED', 'DEACTIVATED'],
                            'key': 'corner_pieces_upstroke',
                            'mode': 'option',
                            'tooltip': 'Corner piece.\n'
                                       'Will be turned in first and swing out, when the upper beam closes.\n'
                                       'This function is used to pass pre-bend flanges.'}
                        ]
        self.update_layout()

    def open(self):
        """ Set OPEN parameters and options and update layout. """
        self._action = Actions.ub_open.key
        self.model.change_action(self._action)
        self.params = [
                        {'label': 'POSITION OPEN',
                            'type': 'TextCtrl',
                            'value': DEFAULT_VALUES[self._action]['position_open'],
                            'mu': 'mm',
                            'key': 'position_open',
                            'mode': 'variable',
                            'tooltip': 'As the upper beam opens, consider:\n'
                                       ' - the additional opening, for corner pieces that are in an inactive position.\n'
                                       ' - that the upper beam should open 10mm in addition to allow a safe '
                                       'parts rotation or unloading.'}
                      ]
        self.options = [{'label': 'NO_WAIT',
                            'type': 'ComboBox',
                            'value': DEFAULT_VALUES[self._action]['no_wait'],
                            'mu': '',
                            'choices': ['ACTIVATED', 'DEACTIVATED'],
                            'key': 'no_wait',
                            'mode': 'option',
                            'tooltip': 'Starts the actual program line.\n'
                                       'The following line will be started also.\n\n'
                                       'Important: Make sure, that you know about the consequences when you use '
                                       'the No. WAIT-function.'},

                        {'label': 'Corner pieces upstroke',
                            'type': 'ComboBox',
                            'value': DEFAULT_VALUES[self._action]['corner_pieces_upstroke'],
                            'mu': '',
                            'choices': ['', 'SWING IN/OUT', 'SWING IN/INACTIVE'],
                            'key': 'corner_pieces_upstroke',
                            'mode': 'option',
                            'tooltip': 'Corner pieces upstroke:\n'
                                       'BLANK: Will not engage the corner tools.\n'
                                       'SWING IN/SWING OUT: Corner pieces swing in for the upper beam upstroke '
                                       'and will swing out into their working position after that.\n'
                                       'SWING IN/INACTIVE: Corner pieces swing in; '
                                       'Remain inactive after the upper beam upstroke.'}]
        self.update_layout()

    def hemming(self):
        """ Set HEMMING parameters and options and update layout. """
        self._action = Actions.hemming.key
        self.model.change_action(self._action)
        self.params = [
                        {'label': 'PULL BACK',
                            'type': 'TextCtrl',
                            'value': DEFAULT_VALUES[self._action]['pull_back'],
                            'mu': 'mm',
                            'key': 'pull_back',
                            'mode': 'variable',
                            'tooltip': 'The pre-bent flange needs to be pulled to a position below '
                                       'the upper tool.\n\n'
                                       'Important: The pull back dimension needs to be negative in the '
                                       'variable definition menu.'},
                        {'label': 'UPPER-BEAM DOWN',
                            'type': 'TextCtrl',
                            'value': DEFAULT_VALUES[self._action]['upper_beam_down'],
                            'mu': 'mm',
                            'key': 'upper_beam_down',
                            'mode': 'variable',
                            'tooltip': 'Closes the upper beam to a dimension for the creation of '
                                       'an open or closed hem.\n'
                                       'Min. 2 x material thickness on material less than 1.25 mm.'},
                        {'label': 'PRESSURE',
                            'type': 'TextCtrl',
                            'value': DEFAULT_VALUES[self._action]['pressure'],
                            'mu': 'mm',
                            'key': 'pressure',
                            'mode': 'variable',
                            'tooltip': 'Pressure.'},
                        {'label': 'UPPER-BEAM UP',
                            'type': 'TextCtrl',
                            'value': DEFAULT_VALUES[self._action]['upper_beam_up'],
                            'mu': 'mm',
                            'key': 'upper_beam_up',
                            'mode': 'variable',
                            'tooltip': 'Opens the upper beam after hemming.'}
                      ]
        self.options = [{'label': 'REDUCED_CLAMP_SPEED',
                            'type': 'ComboBox',
                            'value': DEFAULT_VALUES[self._action]['reduced_clamp_speed'],
                            'mu': '',
                            'choices': ['ACTIVATED', 'DEACTIVATED'],
                            'key': 'reduced_clamp_speed',
                            'mode': 'option',
                            'tooltip': 'Reduced upper beam speed (in %).\n'
                                       'Determined by a parameter (see 8.10.1)'}]
        self.update_layout()

    def update_layout(self):
        """ Update the layout. """
        self.Freeze()
        for row in self.rows:
            self.sizer.Detach(row)
            row.Destroy()
        self.rows = []
        for param in self.params:
            row = Row(self, param)
            self.rows.append(row)
            self.sizer.Add(row, 0, wx.EXPAND | wx.TOP, 1)
        for option in self.options:
            row = Row(self, option)
            self.rows.append(row)
            self.sizer.Add(row, 0, wx.EXPAND | wx.TOP, 1)
        self.Thaw()
        self.GetParent().Layout()

    def __str__(self):
        return self.__class__.__name__

    def __do_layout(self):
        self.sizer = wx.BoxSizer(wx.VERTICAL)

        self.header_obj = Header(self, self.header)
        self.sizer.Add(self.header_obj, 0, wx.EXPAND)

        for param in self.params:
            row = Row(self, param)
            self.rows.append(row)
            self.sizer.Add(row, 0, wx.EXPAND | wx.TOP, 1)
            #self.sizer.Add(wx.StaticLine(self, wx.LI_HORIZONTAL), 0, wx.EXPAND | wx.BOTTOM, 1)
        for option in self.options:
            row = Row(self, option)
            self.rows.append(row)
            self.sizer.Add(row, 0, wx.EXPAND | wx.TOP, 1)
        self.sizer.Add((0, 0), 1, wx.EXPAND)
        self.SetSizer(self.sizer)

    def get_values(self):
        for row in self.rows:
            print(row)

    def get_variables(self):
        """Returns a list of rows of type `variable`."""
        rows_variable = [row for row in self.rows if row.mode == 'variable']
        return rows_variable

    def get_options(self):
        """Returns a list of rows of type `option`."""
        rows_option = [row for row in self.rows if row.mode == 'option']
        return rows_option

    # TODO
    def destroy(self):
        # self.remove_children()
        self.GetParent().remove_action(self)
        self.Destroy()
#------------------------------------------------------------------------------


if __name__ == '__main__':
    app = wx.App()
    frame = wx.Frame(None)
    panel = wx.Panel(frame)
    sizer = wx.BoxSizer(wx.VERTICAL)
    ub = UpperBeam(panel)
    sizer.Add(ub, 0, wx.EXPAND)
    panel.SetSizer(sizer)
    frame.Show()
    ub.get_values()
    app.MainLoop()