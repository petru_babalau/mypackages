# -*- coding: utf-8 -*-
import wx
import action
from utils import Header, Row
from utils import Field


#-------------------------------------------------------------------------
# FOLDING_BEAM HOME
FB_HEIGHT_START = ('00000080', 'xx000000')
FB_HEIGHT_END = ('00000080', 'xx000000')
LINE_END_TOL_2 = ('20', '4e')
NO_WAIT = ('20', '4d')
FB_HOME_POS_DOWN_UP = ('20', '46')

# FOLDING_BEAM BENDING
ANGLE = ('00000080', 'xx000000')
INSIDE_RADIUS = ('00000080', 'xx000000')
THICKNESS = ('00000080', 'xx000000')
REDUCED_FB_SPEED = ('20', '41')

# FOLDING_BEAM UP/DOWN
ANGLE = ('00000080', 'xx000000')
INSIDE_RADIUS = ('00000080', 'xx000000')
THICKNESS = ('00000080', 'xx000000')
FB_HEIGHT_START = ('00000080', 'xx000000')
FB_HEIGHT_END = ('00000080', 'xx000000')
REDUCED_FB_SPEED = ('20', '41')
FB_HOME_POS_DOWN_UP = ('20', '46')

# FOLDING_BEAM UP/DOWN
FB_HEIGHT_START = ('00000080', 'xx000000')
ANGLE_START = ('00000080', 'xx000000')
FB_HEIGHT_END = ('00000080', 'xx000000')
ANGLE_END = ('00000080', 'xx000000')
REDUCED_SPEED = ('20', '41')
PENDULUM_UP_DOWN_BENDING = ('20', '47')
#-------------------------------------------------------------------------


###############################################################################
# Actions
###############################################################################
class Actions:
    home = Field(key='home', label='HOME')
    bending = Field(key='bending', label='BENDING')
    up_down = Field(key='up_down', label='UP/DOWN')
    linear_bending = Field(key='linear_bending', label='LINEAR BENDING')
#-----------------------------------------------------------------------------


HEX_STR = {
    Actions.home.key: '04' + '00' * 9 +
                      '0b' + '00' * 9 +
                      '00000080' * 3 +
                      '{fb_height_start}' +
                      '{fb_height_end}' +
                      '00000080' * 8 +
                      '2020202020' +
                      '{fb_home_pos_down_up}' +
                      '20' * 6 +
                      '{no_wait}' +
                      '{line_end_tol_2}' +
                      '20' * 20 +
                      '00000080' * 4 +
                      '20' * 8,
    Actions.bending.key: '04' + '00' * 9 +
                         '0c' + '00' * 9 +
                         '{angle}' +
                         '00000080' * 4 +
                         '{inside_radius}' +
                         '{thickness}' +
                         '00000080' * 6 +
                         '{reduced_fb_speed}' +
                         '20' * 33 +
                         '00000080' * 4 +
                         '20' * 8,
    Actions.up_down.key: '04' + '00' * 9 +
                         '0d' + '00' * 9 +
                         '{angle}000000' +
                         '00000080' * 2 +
                         '{fb_height_start}' +
                         '{fb_height_end}' +
                         '{inside_radius}' +
                         '{thickness}' +
                         '00000080' * 6 +
                         '{reduced_fb_speed}' +
                         '20202020' +
                         '{fb_home_pos_down_up}' +
                         '20' * 28 +
                         '00000080' * 4 +
                         '20' * 8,
    Actions.linear_bending.key: '04' + '00' * 9 +
                                '0e' + '00' * 9 +
                                '{angle_end}' +
                                '00000080' * 2 +
                                '{fb_height_start}' +
                                '{fb_height_end}' +
                                '00000080' * 8 +
                                '{reduced_speed}' +
                                '2020202020' +
                                '{pendulum_up_down_bending}' +
                                '20' * 27 +
                                '00000080' * 2 +
                                '{angle_start}' +
                                '00000080' +
                                '20' * 8
}


###############################################################################
# Variables
###############################################################################
class Variables:
    fb_height_start = Field(key='fb_height_start', label='FB-HEIGHT START')
    fb_height_end = Field(key='fb_height_end', label='FB-HEIGHT END')
    angle = Field(key='angle', label='ANGLE')
    inside_radius = Field(key='inside_radius', label='INSIDE RADIUS')
    thickness = Field(key='thickness', label='THICKNESS')
    angle_start = Field(key='angle_start', label='ANGLE START')
    angle_end = Field(key='angle_end', label='ANGLE END')
    line_end_tol_2 = Field(key='line_end_tol_2', label='LINE_END_TOL_2')
    no_wait = Field(key='no_wait', label='NO_WAIT')
    fb_home_pos_down_up = Field(key='fb_home_pos_down_up', label='FB_HOME_POS_DOWN_UP')
    reduced_fb_speed = Field(key='reduced_fb_speed', label='REDUCED_FB_SPEED')
    reduced_speed = Field(key='reduced_speed', label='REDUCED_SPEED')
    pendulum_up_down_bending = Field(key='pendulum_up_down_bending',
                                    label='PENDULUM_UP_DOWN_BENDING')
#-----------------------------------------------------------------------------


FIELDS = {
    Actions.home.key: [Variables.fb_height_start.key,
                        Variables.fb_height_end.key,
                        Variables.line_end_tol_2.key,
                        Variables.no_wait.key,
                        Variables.fb_home_pos_down_up.key],
    Actions.bending.key: [Variables.angle.key,
                             Variables.inside_radius.key,
                             Variables.thickness.key,
                             Variables.reduced_fb_speed.key],
    Actions.up_down.key: [Variables.angle.key,
                             Variables.inside_radius.key,
                             Variables.thickness.key,
                             Variables.fb_height_start.key,
                             Variables.fb_height_end.key,
                             Variables.reduced_fb_speed.key,
                             Variables.fb_home_pos_down_up.key],
    Actions.linear_bending.key: [Variables.fb_height_start.key,
                                 Variables.angle_start.key,
                                 Variables.fb_height_end.key,
                                 Variables.angle_end.key,
                                 Variables.reduced_speed.key,
                                 Variables.pendulum_up_down_bending.key]
}


DEFAULT_VALUES = {
    Actions.home.key: {Variables.fb_height_start.key: '0.0',
                         Variables.fb_height_end.key: '0.0',
                        Variables.line_end_tol_2.key: 'DEACTIVATED',
                        Variables.no_wait.key: 'DEACTIVATED',
                        Variables.fb_home_pos_down_up.key: 'DEACTIVATED'},
    Actions.bending.key: {Variables.angle.key: '0.0',
                             Variables.inside_radius.key: '0.0',
                             Variables.thickness.key: '0.0',
                             Variables.reduced_fb_speed.key: 'DEACTIVATED'},
    Actions.up_down.key: {Variables.angle.key: '0.0',
                             Variables.inside_radius.key: '0.0',
                             Variables.thickness.key: '0.0',
                             Variables.fb_height_start.key: '0.0',
                             Variables.fb_height_end.key: '0.0',
                             Variables.reduced_fb_speed.key: 'DEACTIVATED',
                             Variables.fb_home_pos_down_up.key: 'DEACTIVATED'},
    Actions.linear_bending.key: {Variables.fb_height_start.key: '0.0',
                                 Variables.angle_start.key: '0.0',
                                 Variables.fb_height_end.key: '0.0',
                                 Variables.angle_end.key: '0.0',
                                 Variables.reduced_speed.key: 'DEACTIVATED',
                                 Variables.pendulum_up_down_bending.key: 'ACTIVATED'}
}


OPTIONS = {
    Actions.home.key: {'line_end_tol_2': False,
                        'no_wait': False,
                        'fb_home_pos_down': True,
                        'fb_home_pos_up': False},
    Actions.bending.key: {'reduced_fb_speed': False},
    Actions.up_down.key: {'reduced_fb_speed': False,
                            'fb_home_pos_down': True,
                            'fb_home_pos_up': False},
    Actions.linear_bending.key: {'reduced_speed': False,
                                    'pendulum_motion_up_bending': False,
                                    'pendulum_motion_down_bending': False}
}


###############################################################################
# FoldingBeamModel
###############################################################################
class FoldingBeamModel:
    actions = [Actions.home.key,
               Actions.bending.key,
               Actions.up_down.key,
               Actions.linear_bending.key]

    def __init__(self, *args, **kwargs):
        action = kwargs.get('action', self.actions[0])
        self.hex_str = ''
        self._init_action(action)

    def __str__(self):
        string = '<FoldingBeam>'
        for attr in self.__dict__:
            string += '\n'
            string += '\t' + attr + ' = ' + str(getattr(self, attr))
        string += '\n</FoldingBeam>'
        return string

    def _init_action(self, action):
        self.action = action
        self.hex_str = HEX_STR[action]
        for item in DEFAULT_VALUES[action].items():
            setattr(self, item[0], item[1])

    def change_action(self, action=None):
        self.delete_previous_attributes()
        self.reset_hex_str(action)
        self.action = action
        self._init_action(action)

    def reset_hex_str(self, action):
        self.hex_str = HEX_STR[action]

    def delete_previous_attributes(self):
        for attr in FIELDS[self.action]:
            delattr(self, attr)
#-----------------------------------------------------------------------------


def run():
    fb = FoldingBeamModel()
    print(fb)
    fb.change_action(Actions.home.key)
    print(fb)
    fb.change_action(Actions.bending.key)
    print(fb)
    fb.change_action(Actions.up_down.key)
    print(fb)
    fb.change_action(Actions.linear_bending.key)
    print(fb)
    fb.delete_previous_attributes()
    print(fb)


###############################################################################
# Folding beam: Basic functions
#
# 0: Folding beam: Home position
# 1: Folding beam: bending
# 2: Folding beam: UP/DOWN
# 3: Folding beam: Linear bending
#
###############################################################################

HOME = 0
BENDING = 1
UP_DOWN = 2
LINEAR_BENDING = 3


BASIC_FUNC_LABELS = {
    0: 'HOME',
    1: 'BENDING',
    2: 'UP/DOWN',
    3: 'LINEAR BENDING'
}
INV_BASIC_FUNC_LABELS = {v: k for k, v in BASIC_FUNC_LABELS.items()}


class FoldingBeam(wx.Panel, action.Action):
    """ FoldingBeam GUI class."""

    def __init__(self, parent=None, basic_func=HOME):
        """ Default constructor. """
        super(FoldingBeam, self).__init__(parent)
        self.id = id(self)
        self._action = 'home'
        self.model = FoldingBeamModel(action=self._action)
        self.header = {'label': "FOLDING-BEAM",
                        'value': BASIC_FUNC_LABELS[basic_func],
                        'type': 'ComboBox',
                        'choices': list(BASIC_FUNC_LABELS.values()),
                        'tooltip': 'HOME: Moves the folding beam to its starting position.\n\n'
                                   'BENDING: Starts a 3-D bending motion (dynamic edge bending).\n\n'
                                   'UP/DOWN: (CYCLE) Runs a 3-D bending cycle and sets the folding to the '
                                   'new home position after that.\n\n'
                                   'LINEAR BENDING: The swing and height axes of the folding beam work together, so '
                                   'that they can press a hem and bend this hem in one motion.'
        }
        self.params = []
        self.options = []
        self.rows = []
        self.bgcolor = wx.Colour(146, 153, 161)
        self.SetBackgroundColour(self.bgcolor)
        self.SetDoubleBuffered(True)
        self.__do_layout()
        self.__do_binds()

        if basic_func == HOME:
            self.home()
        elif basic_func == BENDING:
            self.bending()
        elif basic_func == UP_DOWN:
            self.up_down()
        elif basic_func == LINEAR_BENDING:
            self.linear_bending()

    def __do_binds(self):
        if self.header['type'] == 'ComboBox':
            getattr(self.header_obj, 'value').Bind(wx.EVT_COMBOBOX, self.__handle_combobox)

    def __handle_combobox(self, event):
        basic_func = getattr(self.header_obj, 'value').GetValue()
        if INV_BASIC_FUNC_LABELS[basic_func] == HOME:
            self.home()
        elif INV_BASIC_FUNC_LABELS[basic_func] == BENDING:
            self.bending()
        elif INV_BASIC_FUNC_LABELS[basic_func] == UP_DOWN:
            self.up_down()
        elif INV_BASIC_FUNC_LABELS[basic_func] == LINEAR_BENDING:
            self.linear_bending()
        event.Skip()

    def home(self):
        """ Set HOME parameters and options and update layout. """
        self._action = Actions.home.key
        self.model.change_action(self._action)
        self.params = [
                        {'label': 'FB-HEIGHT START',
                            'type': 'TextCtrl',
                            'value': DEFAULT_VALUES[self._action]['fb_height_start'],
                            'mu': 'mm',
                            'key': 'fb_height_start',
                            'mode': 'variable',
                            'tooltip': 'Before the folding beam starts to its home position, '
                                       'the folding beam height will be adjusted to this variable.\n\n'
                                       'ATTENTION!\n'
                                       'At folding beam home position up, and opening of the upper beam for parts '
                                       'rotation, the height at the end position of the folding beam must be '
                                       '10mm above the upper beam position.'},
                        {'label': 'FB-HEIGHT END',
                            'type': 'TextCtrl',
                            'value': DEFAULT_VALUES[self._action]['fb_height_end'],
                            'mu': 'mm',
                            'key': 'fb_height_end',
                            'mode': 'variable',
                            'tooltip': 'After the folding beam has reached its home position, '
                                       'the folding beam height will be adjusted to this variable.\n\n'
                                       'ATTENTION!\n'
                                       'At folding beam home position up, and opening of the upper beam for parts '
                                       'rotation, the height at the end position of the folding beam must be '
                                       '10mm above the upper beam position.'}
                      ]
        self.options = [
                        {'label': 'LINE_END_TOL_2',
                            'type': 'ComboBox',
                            'value': DEFAULT_VALUES[self._action]['line_end_tol_2'],
                            'mu': '',
                            'choices': ['ACTIVATED', 'DEACTIVATED'],
                            'key': 'line_end_tol_2',
                            'mode': 'option',
                            'tooltip': 'Starts the folding beam swing, before the folding beam has reached '
                                       'its final height. The production tolerances (folding beam height 2) '
                                       'set the starting time (see 8.10.6 PRODUCTION TOLERANCE.)'},
                        {'label': 'NO_WAIT',
                            'type': 'ComboBox',
                            'value': DEFAULT_VALUES[self._action]['no_wait'],
                            'mu': '',
                            'choices': ['ACTIVATED', 'DEACTIVATED'],
                            'key': 'no_wait',
                            'mode': 'option',
                            'tooltip': 'Starts the actual program line.\n'
                                       'The following line will be started also.\n\n'
                                       'Important: Make sure, that you know about the consequences when you use '
                                       'the NO WAIT-function.'},
                        {'label': 'FB_HOME_POS_DOWN_UP',
                            'type': 'ComboBox',
                            'value': DEFAULT_VALUES[self._action]['fb_home_pos_down_up'],
                            'mu': '',
                            'choices': ['ACTIVATED', 'DEACTIVATED'],
                            'key': 'fb_home_pos_down_up',
                            'mode': 'option',
                            'tooltip': 'Home position Down/Up.\n\n'
                                       'DEACTIVATED: Home position down.\n'
                                       'The folding beam swings to the lower position after bending '
                                       '(about 5 deg.)\n\n'
                                       'ACTIVATED: Home position up.\n'
                                       'The folding beam swings to the upper position after bending '
                                       '(about 175 deg.)'}
                        ]
        self.update_layout()

    def bending(self):
        """ Set BENDING parameters and options and update layout. """
        self._action = Actions.bending.key
        self.model.change_action(self._action)
        self.params = [
                        {'label': 'ANGLE', 'type': 'TextCtrl',
                            'value': DEFAULT_VALUES[self._action]['angle'],
                            'mu': 'deg',
                            'key': 'angle',
                            'mode': 'variable',
                            'tooltip': 'The angle corresponds with the actual folding beam position.\n\n'
                                       'BENDING UP:\n'
                                       'Here the value corresponds with the angle starting from 0 degrees.\n\n'
                                       'BENDING DOWN:\n'
                                       'Here the angle must be subtracted from 180 deg.\n\n'
                                       'ATTENTION!\n'
                                       'Respect the maximum bend angle.'
                         },
                        {'label': 'INSIDE RADIUS', 'type': 'TextCtrl',
                            'value': DEFAULT_VALUES[self._action]['inside_radius'],
                            'mu': 'mm',
                            'key': 'inside_radius',
                            'mode': 'variable',
                            'tooltip': 'Determine the inside radius.'},
                        {'label': 'THICKNESS', 'type': 'TextCtrl',
                            'value': DEFAULT_VALUES[self._action]['thickness'],
                            'mu': 'mm',
                            'key': 'thickness',
                            'mode': 'variable',
                            'tooltip': 'Material thickness (2mm max.)'}
                      ]
        self.options = [{'label': 'REDUCED_FB_SPEED',
                            'type': 'ComboBox',
                            'value': DEFAULT_VALUES[self._action]['reduced_fb_speed'],
                            'mu': '',
                            'choices': ['ACTIVATED', 'DEACTIVATED'],
                            'key': 'reduced_fb_speed',
                            'mode': 'option',
                            'tooltip': 'Reduced folding beam speed (in %).\n'
                                       'Determined by a folding beam parameter (see 8.10.3).'}]
        self.update_layout()

    def up_down(self):
        """ Set UP/DOWN parameters and options and update layout. """
        self._action = Actions.up_down.key
        self.model.change_action(self._action)
        self.params = [
                        {'label': 'ANGLE', 'type': 'TextCtrl',
                            'value': DEFAULT_VALUES[self._action]['angle'],
                            'mu': 'deg',
                            'key': 'angle',
                            'mode': 'variable',
                            'tooltip': 'The angle corresponds with the actual folding beam position.\n\n'
                                       'BENDING UP:\n'
                                       'Here the value corresponds with the angle starting from 0 degrees.\n\n'
                                       'BENDING DOWN:\n'
                                       'Here the angle must be subtracted from 180 deg.\n\n'
                                       'ATTENTION!\n'
                                       'Respect the maximum bend angle.'},
                        {'label': 'INSIDE RADIUS', 'type': 'TextCtrl',
                            'value': DEFAULT_VALUES[self._action]['inside_radius'],
                            'mu': 'mm',
                            'key': 'inside_radius',
                            'mode': 'variable',
                            'tooltip': 'Determine the inside radius.'},
                        {'label': 'THICKNESS', 'type': 'TextCtrl',
                            'value': DEFAULT_VALUES[self._action]['thickness'],
                            'mu': 'mm',
                            'key': 'thickness',
                            'mode': 'variable',
                            'tooltip': 'Material thickness (2mm max.)'},
                        {'label': 'FB-HEIGHT START', 'type': 'TextCtrl',
                            'value': DEFAULT_VALUES[self._action]['fb_height_start'],
                            'mu': 'mm',
                            'key': 'fb_height_start',
                            'mode': 'variable',
                            'tooltip': 'After cycle is finished, the folding beam height is at '
                                       'this position.'},
                        {'label': 'FB-HEIGHT END', 'type': 'TextCtrl',
                            'value': DEFAULT_VALUES[self._action]['fb_height_end'],
                            'mu': 'mm',
                            'key': 'fb_height_end',
                            'mode': 'variable',
                            'tooltip': 'After the folding beam has reached its home position, its '
                                       'height is at this position.'}
                      ]
        self.options = [{'label': 'REDUCED_FB_SPEED',
                            'type': 'ComboBox',
                            'value': DEFAULT_VALUES[self._action]['reduced_fb_speed'],
                            'mu': '',
                            'choices': ['ACTIVATED', 'DEACTIVATED'],
                            'key': 'reduced_fb_speed',
                            'mode': 'option',
                            'tooltip': 'Reduced folding beam speed (in %).\n'
                                       'Determined by a folding beam parameter (see 8.10.3).'},
                        {'label': 'FB_HOME_DOWN_UP',
                            'type': 'ComboBox',
                            'value': DEFAULT_VALUES[self._action]['fb_home_pos_down_up'],
                            'mu': '',
                            'choices': ['ACTIVATED', 'DEACTIVATED'],
                            'key': 'fb_home_pos_down_up',
                            'mode': 'option',
                            'tooltip': 'Home position Down/Up.\n\n'
                                       'DEACTIVATED: Home position down.\n'
                                       'The folding beam swings to the lower position after bending '
                                       '(about 5 deg.)\n\n'
                                       'ACTIVATED: Home position up.\n'
                                       'The folding beam swings to the upper position after bending '
                                       '(about 175 deg.)'}]
        self.update_layout()

    def linear_bending(self):
        """ Set LINEAR BENDING parameters and options and update layout. """
        self._action = Actions.linear_bending.key
        self.model.change_action(self._action)
        self.params = [
                        {'label': 'FB-HEIGHT START', 'type': 'TextCtrl',
                            'value': DEFAULT_VALUES[self._action]['fb_height_start'],
                            'mu': 'mm',
                            'key': 'fb_height_start',
                            'mode': 'variable',
                            'tooltip': 'Start position of the folding beam height before a linear '
                                       'bending function.'},
                        {'label': 'ANGLE START', 'type': 'TextCtrl',
                            'value': DEFAULT_VALUES[self._action]['angle_start'],
                            'mu': 'deg',
                            'key': 'angle_start',
                            'mode': 'variable',
                            'tooltip': 'Angle of the folding beam before a linear bending function.'},
                        {'label': 'FB-HEIGHT END', 'type': 'TextCtrl',
                            'value': DEFAULT_VALUES[self._action]['fb_height_end'],
                            'mu': 'mm',
                            'key': 'fb_height_end',
                            'mode': 'variable',
                            'tooltip': 'End position of the folding beam height after a linear '
                                       'bending function.'},
                        {'label': 'ANGLE END', 'type': 'TextCtrl',
                            'value': DEFAULT_VALUES[self._action]['angle_end'],
                            'mu': 'deg',
                            'key': 'angle_end',
                            'mode': 'variable',
                            'tooltip': 'Angle of the folding beam after a linear bending function.'}
                      ]
        self.options = [{'label': 'REDUCED_SPEED',
                            'type': 'ComboBox',
                            'value': DEFAULT_VALUES[self._action]['reduced_speed'],
                            'mu': '',
                            'choices': ['ACTIVATED', 'DEACTIVATED'],
                            'key': 'reduced_speed',
                            'mode': 'option',
                            'tooltip': 'Reduced folding beam speed (in %).\n'
                                       'Determined by a folding beam parameter (see 8.10.3).'},
                        {'label': 'PENDULUM_UP_DOWN_BENDING',
                            'type': 'ComboBox',
                            'value': DEFAULT_VALUES[self._action]['pendulum_up_down_bending'],
                            'mu': '',
                            'choices': ['ACTIVATED', 'DEACTIVATED'],
                            'key': 'pendulum_up_down_bending',
                            'mode': 'option',
                            'tooltip': 'ACTIVATED: Position the pendulum axis for up bending.\n\n'
                                       'DEACTIVATED: Position the pendulum axis for down bending.'}]
        self.update_layout()

    def update_layout(self):
        """ Update the layout. """
        self.Freeze()
        for row in self.rows:
            self.sizer.Detach(row)
            row.Destroy()
        self.rows = []
        for param in self.params:
            row = Row(self, param)
            self.rows.append(row)
            self.sizer.Add(row, 0, wx.EXPAND | wx.TOP, 1)
        for option in self.options:
            row = Row(self, option)
            self.rows.append(row)
            self.sizer.Add(row, 0, wx.EXPAND | wx.TOP, 1)
        self.Thaw()
        self.GetParent().Layout()

    def __str__(self):
        return self.__class__.__name__

    def __do_layout(self):
        self.sizer = wx.BoxSizer(wx.VERTICAL)

        self.header_obj = Header(self, self.header)
        self.sizer.Add(self.header_obj, 0, wx.EXPAND)

        for param in self.params:
            row = Row(self, param)
            self.rows.append(row)
            self.sizer.Add(row, 0, wx.EXPAND | wx.TOP, 1)
            #self.sizer.Add(wx.StaticLine(self, wx.LI_HORIZONTAL), 0, wx.EXPAND | wx.BOTTOM, 1)
        for option in self.options:
            row = Row(self, option)
            self.rows.append(row)
            self.sizer.Add(row, 0, wx.EXPAND | wx.TOP, 1)
        self.sizer.Add((0, 0), 1, wx.EXPAND)
        self.SetSizer(self.sizer)

    def get_values(self):
        for row in self.rows:
            print(row)

    def get_variables(self):
        """Returns a list of rows of type `variable`."""
        rows_variable = [row for row in self.rows if row.mode == 'variable']
        return rows_variable

    def get_options(self):
        """Returns a list of rows of type `option`."""
        rows_option = [row for row in self.rows if row.mode == 'option']
        return rows_option

    # TODO
    def destroy(self):
        # self.remove_children()
        self.GetParent().remove_action(self)
        self.Destroy()
#-----------------------------------------------------------------------------


if __name__ == '__main__':
    app = wx.App()
    frame = wx.Frame(None)
    panel = wx.Panel(frame)
    sizer = wx.BoxSizer(wx.VERTICAL)
    manipulator = FoldingBeam(panel)
    sizer.Add(manipulator, 0, wx.EXPAND)
    panel.SetSizer(sizer)
    frame.Show()
    manipulator.get_values()
    app.MainLoop()