

FLANGE = '00000080' # aa000000
ANGLE = '00000080' # ab000000
FB_HEIGHT_START = '00000080' # ac000000
FB_HEIGHT_END = '00000080' # ad000000
UB_HEIGHT_DOWN = '00000080' # ae000000
UB_HEIGHT_UP = '00000080' # af000000
STEP_SIZE = '00000080' # b0000000
STEP_NO = '00000080' # b1000000
FB_HEIGHT_END_2 = '00000080' # b2000000
UB_HEIGHT_UP_2 = '00000080' # b3000000
FREE = '00000080' # b4000000

CORNER_PIECE_WORKING_POSITION = ('20', '4b')


hex_string = '05000000000000000000' \
             '11000000000000000000' \
             + ANGLE \
             + UB_HEIGHT_DOWN \
             + UB_HEIGHT_UP \
             + FB_HEIGHT_START \
             + FB_HEIGHT_END \
             + FREE +\
             '00000080 00000080 00000080' \
             + STEP_SIZE +\
             '00000080' \
             + FLANGE \
             + UB_HEIGHT_UP_2 +\
             '20202020 20202020' \
             '49' \
             '20' \
             + CORNER_PIECE_WORKING_POSITION[0] +\
             '4c' \
             '20202020 20202020 20202020 20202020 20202020 2020' \
             '00000080 00000080' \
             + STEP_NO\
             + FB_HEIGHT_END_2 +\
             '20202020 20202020'
