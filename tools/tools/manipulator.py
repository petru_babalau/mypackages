# -*- coding: utf-8 -*-
import wx
import action
from utils import Header, Row
from utils import Field


#-------------------------------------------------------------------------
# Manipulator POSITION/ROTATE
ROTATION_POS = ('00000080', 'xx000000')
ROTATION_ANGLE = ('00000080', 'xx000000')
POSITION = ('00000080', 'xx000000')
REDUCED_ROTATIONAL_SPEED = ('20', '44')
REDUCED_LINEAR_SPEED = ('20', '45')
REQUEST_FOR_NEW_BLANK = ('20', '43')

# Manipulator LOAD
LOADING_POSITION = ('00000080', 'xx000000')
BLANK_DEF = ('00000080', 'xx000000')

# Manipulator UNLOAD
ROTATION_POS = ('00000080', 'xx000000')
ROT_ANGLE = ('00000080', 'xx000000')
UNLOAD_POSITION = ('00000080', 'xx000000')
UNLODING_BELT = ('20', '43')

# Manipulator REPOSITION
PULL_BACK = ('00000080', 'xx000000')
UPPER_BEAM_DOWN = ('00000080', 'xx000000')
OFFSET = ('00000080', 'xx000000')
UPPER_BEAM_UP = '00'
HALF_UP = ('20', '43')
#-----------------------------------------------------------------------------


###############################################################################
# Actions
###############################################################################
class Actions:
    """ Actions """
    position_rotate = Field(key='position_rotate', label='POSITION ROTATE')
    load = Field(key='load', label='LOAD')
    unload = Field(key='unload', label='UNLOAD')
    reposition = Field(key='reposition', label='REPOSITION')
#-----------------------------------------------------------------------------


HEX_STR = {
    Actions.position_rotate.key: '02' + '00' * 9 +
                                 '04' + '00' * 9 +
                                 '00000080' * 7 +
                                 '{rotation_pos}' +
                                 '{rotation_angle}' +
                                 '{position}' +
                                 '00000080' * 3 +
                                 '2020' +
                                 '{request_for_new_blank}' +
                                 '{reduced_rotational_speed}' +
                                 '{reduced_linear_speed}' +
                                 '20' * 29 +
                                 '00000080' * 4 +
                                 '20' * 8,
    Actions.load.key: '02' + '00' * 9 +
                      '05' + '00' * 9 +
                      '00000080' * 9 +
                      '{loading_position}' +
                      '00000080' * 3 +
                      '20' * 34 +
                      '00000080' * 2 +
                      '{blank_def}' +
                      '00000080' +
                      '20' * 8,
    Actions.unload.key: '02' + '00' * 9 +
                        '06' + '00' * 9 +
                        '00000080' * 7 +
                        '{rotation_pos}' +
                        '{rotation_angle}' +
                        '{unload_position}' +
                        '00000080' * 3 +
                        '2020' +
                        '{unloading_belt}' +
                        '20' * 31 +
                        '00000080' * 4 +
                        '20' * 8,
    Actions.reposition.key: '02' + '00' * 9 +
                            '07' + '00' * 9 +
                            '00000080' +
                            '{upper_beam_down}' +
                            '{upper_beam_up}' +
                            '00000080' * 6 +
                            '{pull_back}' +
                            '{offset}000080' +    # ?????
                            '00000080' * 2 +
                            '2020' +
                            '{half_up}' +
                            '20' * 31 +
                            '00000080' * 4 +
                            '20' * 8
}


###############################################################################
# Variables
###############################################################################
class Variables:
    """ Variables """
    rotation_position = Field(key='rotation_pos', label='ROTATION POSITIONS')
    rotation_angle = Field(key='rotation_angle', label='ROTATION ANGLE')
    position = Field(key='position', label='POSITION')
    loading_position = Field(key='loading_position', label='LOADING POSITION')
    blank_def = Field(key='blank_def', label='BLANK DEF(BLK)')
    unload_position = Field(key='unload_position', label='UNLOAD POSITION')
    pull_back = Field(key='pull_back', label='PULL BACK')
    upper_beam_down = Field(key='upper_beam_down', label='UPPER-BEAM DOWN')
    offset = Field(key='offset', label='OFFSET')
    upper_beam_up = Field(key='upper_beam_up', label='UPPER-BEAM UP')
    reduced_rotational_speed = Field(key='reduced_rotational_speed',
                                        label='REDUCED_ROTATIONAL_SPEED')
    reduced_linear_speed = Field(key='reduced_linear_speed', label='REDUCED_LINEAR_SPEED')
    request_new_blank = Field(key='request_new_blank', label='REQUEST_FOR_NEW_BLANK')
    unloading_belt = Field(key='unloading_belt', label='UNLOADING_BELT')
    half_up = Field(key='half_up', label='HALF_UP')
#-----------------------------------------------------------------------------


FIELDS = {
    Actions.position_rotate.key: [Variables.rotation_position.key,
                                     Variables.rotation_angle.key,
                                     Variables.position.key,
                                     Variables.reduced_rotational_speed.key,
                                     Variables.reduced_linear_speed.key,
                                     Variables.request_new_blank.key],
    Actions.load.key: [Variables.loading_position.key,
                        Variables.blank_def.key],
    Actions.unload.key: [Variables.rotation_position.key,
                         Variables.rotation_angle.key,
                         Variables.unload_position.key,
                         Variables.unloading_belt.key],
    Actions.reposition.key: [Variables.pull_back.key,
                             Variables.upper_beam_down.key,
                             Variables.offset.key,
                             Variables.upper_beam_up.key,
                             Variables.half_up.key]
}


DEFAULT_VALUES = {
    Actions.position_rotate.key: {Variables.rotation_position.key: '',
                                     Variables.rotation_angle.key: '',
                                     Variables.position.key: '',
                                     Variables.reduced_rotational_speed.key: 'DEACTIVATED',
                                     Variables.reduced_linear_speed.key: 'DEACTIVATED',
                                     Variables.request_new_blank.key: 'DEACTIVATED'},
    Actions.load.key: {Variables.loading_position.key: '',
                         Variables.blank_def.key: ''},
    Actions.unload.key: {Variables.rotation_position.key: '',
                         Variables.rotation_angle.key: '',
                         Variables.unload_position.key: '',
                         Variables.unloading_belt.key: 'DEACTIVATED'},
    Actions.reposition.key: {Variables.pull_back.key: '',
                             Variables.upper_beam_down.key: '',
                             Variables.offset.key: '',
                             Variables.upper_beam_up.key: '',
                             Variables.half_up.key: 'DEACTIVATED'}
}


OPTIONS = {
    Actions.position_rotate.key: {'reduced_rotational_speed': False,
                                 'reduced_linear_speed': False,
                                 'request_for_new_blank': False},
    Actions.load.key: {},
    Actions.unload.key: {'unloading_belt': False},
    Actions.reposition.key: {'half_up': False}
}

# TODO:
HEX_VALUES = {}


###############################################################################
# ManipulatorModel
###############################################################################
class ManipulatorModel:
    """ Manipulator model class"""
    actions = [Actions.position_rotate.key,
               Actions.load.key,
               Actions.unload.key,
               Actions.reposition.key]

    def __init__(self, *args, **kwargs):
        action = kwargs.get('action', self.actions[0])
        self.hex_str = ''
        self.init_action(action)

    def __str__(self):
        string = '<Manipulator>'
        for attr in self.__dict__:
            string += '\n'
            string += '\t' + attr + ' = ' + str(getattr(self, attr))
        string += '\n</Manipulator>'
        return string

    def init_action(self, action):
        """ Init hex string & set attributes. """
        self.action = action
        self.hex_str = HEX_STR[action]
        for item in DEFAULT_VALUES[action].items():
            setattr(self, item[0], item[1])

    def formatted_hex_str(self, data={}):
        """ Returns a formated hex string. """
        formatted_hex_str = self.hex_str.format(**data)
        return formatted_hex_str

    def change_action(self, action=None):
        """ Change the main action. """
        self.delete_attributes()
        self.set_hex_str(action)
        self.action = action
        self.init_action(action)

    def set_hex_str(self, action):
        """ Set hex string """
        self.hex_str = HEX_STR[action]

    def delete_attributes(self):
        """ Delete the attributes of the model. """
        for attr in FIELDS[self.action]:
            delattr(self, attr)
#-----------------------------------------------------------------------------


def run():
    manip = ManipulatorModel()
    print(manip)
    manip.change_action(Actions.load.key)
    print(manip)
    manip.change_action(Actions.unload.key)
    print(manip)
    manip.change_action(Actions.reposition.key)
    print(manip)
    manip.change_action(Actions.position_rotate.key)
    print(manip)
    manip.delete_attributes()
    print(manip)



###############################################################################
# Manipulator: Basic functions
#
# 0: Position and rotate main manipulator
# 1: Load main manipulator
# 2: Unload main manipulator
# 3: Reposition main manipulator
#
###############################################################################

POSITION_ROTATE = 0
LOAD = 1
UNLOAD = 2
REPOSITION = 3

BASIC_FUNC_LABELS = {
    0: 'POSITION ROTATE',
    1: 'LOAD',
    2: 'UNLOAD',
    3: 'REPOSITION'
}
INV_BASIC_FUNC_LABELS = {v: k for k, v in BASIC_FUNC_LABELS.items()}


class Manipulator(wx.Panel, action.Action):
    """ Manipulator GUI class """

    def __init__(self, parent=None, basic_func=POSITION_ROTATE):
        super(Manipulator, self).__init__(parent)
        self.id = id(self)
        self._action = 'position_rotate'
        self.model = ManipulatorModel(action=self._action)
        self.header = {'label': "MANIPULATOR",
                        'value': BASIC_FUNC_LABELS[basic_func],
                        'type': 'ComboBox',
                        'choices': list(BASIC_FUNC_LABELS.values()),
                        'tooltip': 'POSITION-ROTATE: Blank can be: positioned, rotated, positioned & rotated.\n\n'
                                   'LOAD: Loading position for the main manipulator (takes '
                                   'the blank from the loading manipulator).\n\n'
                                   'UNLOAD: Unloades the finished part to the transport belts.\n\n'
                                   'REPOSITION: Repositions the main manipulator for small blanks.\n'
                                   'Required after rotating the part to the long side, if the main manipulator '
                                   'cannot reach the position, because:\n'
                                   ' - the bending position is to close, or\n'
                                   ' - the free move for the last flange is not possible.'
        }
        self.params = []
        self.options = []
        self.rows = []
        self.bgcolor = wx.Colour(146, 153, 161)
        self.SetBackgroundColour(self.bgcolor)
        self.SetDoubleBuffered(True)
        self.__do_layout()
        self.__do_binds()

        if basic_func == POSITION_ROTATE:
            self.position_rotate()
        elif basic_func == LOAD:
            self.load()
        elif basic_func == UNLOAD:
            self.unload()
        elif basic_func == REPOSITION:
            self.reposition()

    def __do_binds(self):
        if self.header['type'] == 'ComboBox':
            getattr(self.header_obj, 'value').Bind(wx.EVT_COMBOBOX, self.__handle_combobox)

    def __handle_combobox(self, event):
        basic_func = getattr(self.header_obj, 'value').GetValue()
        if INV_BASIC_FUNC_LABELS[basic_func] == POSITION_ROTATE:
            self.position_rotate()
            self.get_values()
        elif INV_BASIC_FUNC_LABELS[basic_func] == LOAD:
            self.load()
            self.get_values()
        elif INV_BASIC_FUNC_LABELS[basic_func] == UNLOAD:
            self.unload()
            self.get_values()
        elif INV_BASIC_FUNC_LABELS[basic_func] == REPOSITION:
            self.reposition()
            self.get_values()

        event.Skip()

    def position_rotate(self):
        """ Set POSITION/ROTATE parameters and options and update layout. """
        self._action = Actions.position_rotate.key
        self.model.change_action(self._action)
        self.params = [
                        {'label': 'ROTATION POS',
                            'type': 'TextCtrl',
                            'value': DEFAULT_VALUES[self._action]['rotation_pos'],
                            'mu': 'mm',
                            'key':'rotation_pos',
                            'mode': 'variable',
                            'tooltip': 'If there is no input, the CNC calculates the rotating position automatically.\n'
                                       'Input only required if there is a need for a specified rotating position.\n'
                                       'This might be possible for very large parts loaded with a big offset.\n'
                                       'The dimension is related to the bend line and the main manipulator position.'
                         },
                        {'label': 'ROTATION ANGLE',
                            'type': 'TextCtrl',
                            'value': DEFAULT_VALUES[self._action]['rotation_angle'],
                            'mu': 'deg',
                            'key': 'rotation_angle',
                            'mode': 'variable',
                            'tooltip': 'Enter the rotation angle or blank side.\n'
                                       'Therefor:\n'
                                       '   Side 1 = -90 deg\n'
                                       '   Side 2 = +90 deg\n'
                                       '   Side 3 = 180 deg\n'
                                       '   Side 4 = 0 deg'},
                        {'label': 'POSITION',
                            'type': 'TextCtrl',
                            'value': DEFAULT_VALUES[self._action]['position'],
                            'mu': 'mm',
                            'key': 'position',
                            'mode': 'variable',
                            'tooltip': 'Main manipulator position (flange dimensions).\n'
                                       'If possibile, enter the first flange dimension in the same\n'
                                       'program line where the manipulator rotates the part.\n'
                                       'The main manipulator will rotate and position the part at a time.\n'
                                       'The flange dimension should be the dimension beyond of the bend line.'}
                      ]
        self.options = [
                        {'label': 'REDUCED_ROTATIONAL_SPEED',
                            'type': 'ComboBox',
                            'value': DEFAULT_VALUES[self._action]['reduced_rotational_speed'],
                            'mu': '',
                            'choices': ['ACTIVATED', 'DEACTIVATED'],
                            'key': 'reduced_rotational_speed',
                            'mode': 'option',
                            'tooltip': 'Reduced rotating speed (in %).\n'
                                       'Value set in the program parameters(see 8.10.4)'},
                        {'label': 'REDUCED_LINEAR_SPEED',
                            'type': 'ComboBox',
                            'value': DEFAULT_VALUES[self._action]['reduced_linear_speed'],
                            'mu': '',
                            'choices': ['ACTIVATED', 'DEACTIVATED'],
                            'key': 'reduced_linear_speed',
                            'mode': 'option',
                            'tooltip': 'Reduced positionning speed (in %).\n'
                                       'Value set in the program parameters(see 8.10.4).'},
                        {'label': 'REQUEST_NEW_BLANK',
                            'type': 'ComboBox',
                            'value': DEFAULT_VALUES[self._action]['request_new_blank'],
                            'mu': '',
                            'choices': ['ACTIVATED', 'DEACTIVATED'],
                            'key': 'request_new_blank',
                            'mode': 'option',
                            'tooltip': 'Request for new blank.\n'
                                       'This allows the following blank to be brought into '
                                       'the main manipulator area, while the actual part is still in production.\n'
                                       'This is permitted if:\n'
                                       ' - the blank width on both parts is less than 1000 mm.\n'
                                       ' - the last bending side of the actual part is a long side.'}
                        ]
        self.update_layout()

    def load(self):
        """ Set LOAD parameters and options and update layout. """
        self._action = Actions.load.key
        self.model.change_action(self._action)
        self.params = [
                        {'label': 'LOADING POSITION',
                            'type': 'TextCtrl',
                            'value': DEFAULT_VALUES[self._action]['loading_position'],
                            'mu': 'mm',
                            'key': 'loading_position',
                            'mode': 'variable',
                            'tooltip': 'Blank loading position(see 8.5.2).'},
                        {'label': 'BLANK DEF(BLK)',
                            'type': 'TextCtrl',
                            'value': DEFAULT_VALUES[self._action]['blank_def'],
                            'mu': '??',
                            'key': 'blank_def',
                            'mode': 'variable',
                            'tooltip': 'Enter starting of the blank definition variables (block).\n\n'
                                       'They are:\n'
                                       '   v1 = blank length\n'
                                       '   v2 = blank width\n'
                                       '   v3 = material thickness\n'
                                       '   v4 = thickness check dimension\n\n'
                                       'These 4 variables must come behind each other.\n'
                                       'Therefore only the first variable(blank length v1) needs to\n'
                                       'be set, the other 3 variables will be loaded automatically(v2 - v4).'}
                      ]
        self.options = []
        self.update_layout()

    def unload(self):
        """ Set UNLOAD parameters and options and update layout. """
        self._action = Actions.unload.key
        self.model.change_action(self._action)
        self.params = [
                        {'label': 'ROTATION POS',
                            'type': 'TextCtrl',
                            'value': DEFAULT_VALUES[self._action]['rotation_pos'],
                            'mu': 'mm',
                            'key': 'rotation_pos',
                            'mode': 'variable',
                            'tooltip': 'Input only, if the finished part needs to be rotated at a certain\n'
                                       'position for unloading.\n'
                                       'As soon as a fix position is enterd, the automated colision\n'
                                       'check is not active any more.'},
                        {'label': 'ROTATION ANGLE',
                            'type': 'TextCtrl',
                            'value': DEFAULT_VALUES[self._action]['rotation_angle'],
                            'mu': 'deg',
                            'key': 'rotation_angle',
                            'mode': 'variable',
                            'tooltip': 'Without an input the finished part will be just pulled back for\n'
                                       'unloading.\n'
                                       'Input only, if different unloading angle is required.'},
                        {'label': 'UNLOAD POSITION',
                            'type': 'TextCtrl',
                            'value': DEFAULT_VALUES[self._action]['unload_position'],
                            'mu': 'mm',
                            'key': 'unload_position',
                            'mode': 'variable',
                            'tooltip': 'Input the unloading position only, if:\n'
                                       ' - a main manipulator offset needs to be considered.\n'
                                       ' - small parts need to be unloaded with only a single belt.\n'
                                       '(see 8.5.2 and 8.5.4)'}
                      ]
        self.options = [{'label': 'UNLOADING_BELT',
                            'type': 'ComboBox',
                            'value': DEFAULT_VALUES[self._action]['unloading_belt'],
                            'mu': '',
                            'choices': ['ACTIVATED', 'DEACTIVATED'],
                            'key': 'unloading_belt',
                            'mode': 'option',
                            'tooltip': 'Lifts the transport belts 40mm above the sheet supports.\n\n'
                                       'If this softkey is not activated, the belts come up for 15mm only.'}]
        self.update_layout()

    def reposition(self):
        """ Set REPOSITION parameters and options and update layout. """
        self._action = Actions.reposition.key
        self.model.change_action(self._action)
        self.params = [
                        {'label': 'PULL BACK',
                            'type': 'TextCtrl',
                            'value': DEFAULT_VALUES[self._action]['pull_back'],
                            'mu': 'mm',
                            'key': 'pull_back',
                            'mode': 'variable',
                            'tooltip': 'The variable determines the distance, the outside edge of\n'
                                       'the blank needs to be pulled back behind the bend line, so\n'
                                       'that the upper tool can hold it.\n\n'
                                       'Important: The repositioning offset value needs to be negative\n'
                                       'in the variable definition menu.'},
                        {'label': 'UPPER-BEAM DOWN',
                            'type': 'TextCtrl',
                            'value': DEFAULT_VALUES[self._action]['upper_beam_down'],
                            'mu': 'mm',
                            'key': 'upper_beam_down',
                            'mode': 'variable',
                            'tooltip': 'Upper beam clamping position for repositioning (v81)\n'
                                       'E.g. 0.2mm less than the material thickness.'
                         },
                        {'label': 'OFFSET',
                            'type': 'TextCtrl',
                            'value': DEFAULT_VALUES[self._action]['offset'],
                            'mu': 'mm',
                            'key': 'offset',
                            'mode': 'variable',
                            'tooltip': 'Offset dimension for the main manipulator. Maximum offset\n'
                                       'limited by the inside flange of the opposite side.'},
                        {'label': 'UPPER-BEAM UP',
                            'type': 'TextCtrl',
                            'value': DEFAULT_VALUES[self._action]['upper_beam_up'],
                            'mu': 'mm',
                            'key': 'upper_beam_up',
                            'mode': 'variable',
                            'tooltip': 'Upper beam opens to this dimension (v77) after\n'
                                       'repositioning.'}
                      ]
        self.options = [{'label': 'HALF_UP',
                            'type': 'ComboBox',
                            'value': DEFAULT_VALUES[self._action]['half_up'],
                            'mu': '',
                            'choices': ['ACTIVATED', 'DEACTIVATED'],
                            'key': 'half_up',
                            'mode': 'option',
                            'tooltip': 'Half up.'}]
        self.update_layout()

    def update_layout(self):
        """ Update the layout. """
        self.Freeze()
        for row in self.rows:
            self.sizer.Detach(row)
            row.Destroy()
        self.rows = []
        for param in self.params:
            row = Row(self, param)
            self.rows.append(row)
            self.sizer.Add(row, 0, wx.EXPAND | wx.TOP, 1)
        for option in self.options:
            row = Row(self, option)
            self.rows.append(row)
            self.sizer.Add(row, 0, wx.EXPAND | wx.TOP, 1)
        self.Thaw()
        self.GetParent().Layout()

    def __str__(self):
        return "{}: {}".format(self.__class__.__name__, self._action)

    def __do_layout(self):
        self.sizer = wx.BoxSizer(wx.VERTICAL)

        self.header_obj = Header(self, self.header)
        self.sizer.Add(self.header_obj, 0, wx.EXPAND)

        for param in self.params:
            row = Row(self, param)
            self.rows.append(row)
            self.sizer.Add(row, 0, wx.EXPAND | wx.TOP, 1)
            #self.sizer.Add(wx.StaticLine(self, wx.LI_HORIZONTAL), 0, wx.EXPAND | wx.BOTTOM, 1)
        for option in self.options:
            row = Row(self, option)
            self.rows.append(row)
            self.sizer.Add(row, 0, wx.EXPAND | wx.TOP, 1)

        #self.sizer.Add((0, 0), 1, wx.EXPAND)
        self.SetSizer(self.sizer)

    def get_values(self):
        for row in self.rows:
            print(row)

    def get_variables(self):
        """Returns a list of rows of type `variable`."""
        rows_variable = [row for row in self.rows if row.mode == 'variable']
        return rows_variable

    def get_options(self):
        """Returns a list of rows of type `option`."""
        rows_option = [row for row in self.rows if row.mode == 'option']
        return rows_option

    def get_string(self, variables_dict):
        return self.model.formatted_hex_str(variables_dict)

    # TODO
    def destroy(self):
        """ Destroy children & self. """
        # self.remove_children()
        self.GetParent().remove_action(self)
        self.Destroy()
#-----------------------------------------------------------------------------


if __name__ == '__main__':
    app = wx.App()
    frame = wx.Frame(None)
    panel = wx.Panel(frame)
    sizer = wx.BoxSizer(wx.VERTICAL)
    manipulator = Manipulator(panel)
    sizer.Add(manipulator, 0, wx.EXPAND)
    panel.SetSizer(sizer)
    frame.Show()
    manipulator.get_values()
    app.MainLoop()