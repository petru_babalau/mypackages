import wx
import action
from utils import Header, Row
from utils import Field


UB_CHANGE_POS = '00000080'  # 'aa000000'
UPPER_BEAM = '00000080'     # 'ab000000'
LEFT_ELBOW_TOOL = ('20', '44')
RIGHT_ELBOW_TOOL = ('20', '45')
CORNER_PIECE_WORKING_POSITION = ('20', '4b')
ELBOW_TOOL_UP_DOWN = ('20', '41') # Elbow tool Up; Elbow tool down;
CORNER_PIECE_SWING_IN_OUT = ('20', '4c')
CORNER_PIECE_SWING_IN_INACTIVE = ('20', '49')


aux_string = ''.join(['2020', CORNER_PIECE_WORKING_POSITION[0], CORNER_PIECE_SWING_IN_OUT[0]])
if CORNER_PIECE_SWING_IN_INACTIVE:
    aux_string = ''.join(['49', '20', CORNER_PIECE_WORKING_POSITION[0], '4c'])
elif CORNER_PIECE_SWING_IN_OUT:
    aux_string = ''.join(['2020', CORNER_PIECE_WORKING_POSITION[0], '4c'])

hex_string = '06000000000000000000' \
             '14000000000000000000' \
             '0000008 000000080' \
             + UPPER_BEAM +\
             '00000080 00000080 00000080 00000080' \
             '00000080 00000080 00000080 00000080' \
             + UB_CHANGE_POS +\
             '00000080' \
             + ELBOW_TOOL_UP_DOWN[0] +\
             '2020' \
             + LEFT_ELBOW_TOOL[0] +\
             + RIGHT_ELBOW_TOOL[0] +\
             '202020' \
             + aux_string +\
             '202020 20202020 20202020 20202020' \
             '20202020 202020' \
             '00000080 00000080 00000080 00000080' \
             '20202020 20202020'
