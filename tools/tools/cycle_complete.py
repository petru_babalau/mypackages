

FLANGE = '00000080' # aa000000
UB_HEIGHT_DOWN = '00000080' # ab000000
ANGLE = '00000080' # ac000000
INSIDE_RADIUS = '00000080' # ad000000
THICKNESS = '00000080' # 'ae000000'
FB_HEIGHT_START = '00000080' # af000000
FB_HEIGHT_END = '00000080' # b0000000
UB_HEIGHT_UP = '00000080' # b1000000
FREE = '00000080' # b2000000
REDUCED_MAIN_MANIP_POS_SPEED = ('20', '45')
CORNER_PIECE_WORKING_POSITION = ('20', '4b')
FB_HOME_DOWN_UP = ('20', '46')
CORNER_PIECE_SWING_IN_OUT = ('20', '4c')
CORNER_PIECE_SWING_IN_INACTIVE = ('20', '49')
LINE_END_TOL_2 = ('20', '4e')
NO_HOME = ('20', '48')
UB_REDUCED_SPEED = ('20', '42')
FB_REDUCED_SPEED = ('20', '41')


aux_string = ''.join(['2020', CORNER_PIECE_WORKING_POSITION[0], CORNER_PIECE_SWING_IN_OUT[0]])
if CORNER_PIECE_SWING_IN_INACTIVE:
    aux_string = ''.join(['49', '20', CORNER_PIECE_WORKING_POSITION[0], '4c'])
elif CORNER_PIECE_SWING_IN_OUT:
    aux_string = ''.join(['2020', CORNER_PIECE_WORKING_POSITION[0], '4c'])


hex_string = '05000000000000000000' \
             '0f000000000000000000' \
             + ANGLE \
             + UB_HEIGHT_DOWN \
             + UB_HEIGHT_UP \
             + FB_HEIGHT_START \
             + FB_HEIGHT_END \
             + INSIDE_RADIUS \
             + THICKNESS +\
             '00000080 00000080' \
             + FLANGE +\
             '00000080 00000080 00000080' \
             + FB_REDUCED_SPEED[0] \
             + UB_REDUCED_SPEED[0] +\
             '2020' \
             + REDUCED_MAIN_MANIP_POS_SPEED[0] \
             + FB_HOME_DOWN_UP[0] +\
             '20' \
             + NO_HOME \
             + aux_string \
             + LINE_END_TOL_2 +\
             '202020 20202020 20202020' \
             '20202020 20202020 2020' \
             '00000080 00000080' \
             + FREE +\
             '00000080' \
             '20202020 s20202020'