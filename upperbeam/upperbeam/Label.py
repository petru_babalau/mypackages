# -*- coding: utf-8 -*-
import wx

MAIN_BG_COLOR = wx.Colour(50, 61, 67)
MAIN_FG_COLOR = wx.Colour(255, 255, 255)
LINE_BG_COLOR = wx.Colour(94, 188, 255)
FONT_FACENAME = "Seagoe UI"
FONT_WEIGHT = wx.FONTWEIGHT_NORMAL

LINE_WEIGHT = 1


#########################################################################################
class LabelEvent(wx.PyCommandEvent):
    def __init__(self, eventType, eventId):
        wx.PyCommandEvent.__init__(self, eventType, eventId)
        self.isDown = False
        self.theButton = None

    def SetIsDown(self, isDown):
        """
        :param isDown:
        """
        self.isDown = isDown

    def GetIsDown(self):
        return self.isDown

    def SetButtonObj(self, btn):
        self.theButton = btn

    def GetButtonObj(self):
        return self.theButton


class Label(wx.PyControl):
    version = '0.1'

    def __init__(self, parent=None,
                 text="",
                 index=0,
                 font_size=12,
                 bgcolor=MAIN_BG_COLOR,
                 color=MAIN_FG_COLOR,
                 font_weight=FONT_WEIGHT,
                 font_face=FONT_FACENAME,
                 padding=(0, 0, 0, 0),
                 size=(-1, -1),
                 align="MIDDLE"):

        super(Label, self).__init__(parent=parent, style=wx.NO_BORDER)

        self.index = index
        self.text = text
        self.font = wx.Font(pointSize=font_size, family=wx.DEFAULT, style=wx.NORMAL,
                            weight=font_weight, faceName=font_face)
        t_text = wx.StaticText(self, id=wx.ID_ANY, label=self.text)
        self.text_size = t_text.GetClientSize()
        t_text.Destroy()

        self.font_color = color
        self.background_color = bgcolor
        self.align = align
        self.padding = padding
        self.size = size

        self.Bind(wx.EVT_PAINT, self.on_paint)
        self.Bind(wx.EVT_SIZE, self.on_size)
        self.Bind(wx.EVT_ERASE_BACKGROUND, self.on_erase_background)

    def on_size(self, event):
        self.Refresh()

    def DoGetBestSize(self):
        """
            Overridden base class virtual.  Determines the best size of the control
            based on the label size, the bitmap size and the current font.
        """

        # Retrieve our properties: the text label, the font and the check
        # bitmap
        label = self.text
        font = self.font

        if not font:
            # No font defined? So use the default GUI font provided by the system
            font = wx.SystemSettings.GetFont(wx.SYS_DEFAULT_GUI_FONT)

        # Set up a wx.ClientDC. When you don't have a dc available (almost
        # always you don't have it if you are not inside a wx.EVT_PAINT event),
        # use a wx.ClientDC (or a wx.MemoryDC) to measure text extents
        dc = wx.ClientDC(self)
        dc.SetFont(font)

        # Measure our label
        textWidth, textHeight = dc.GetTextExtent(label)

        # Retrieve the check bitmap dimensions
        W, H = self.GetClientSize()

        # Ok, we're almost done: the total width of the control is simply
        # the sum of the bitmap width, the spacing and the text width,
        # while the height is the maximum value between the text width and
        # the bitmap width
        totalWidth = textWidth + self.padding[3]
        totalHeight = textHeight + 10

        if self.size[0] != -1:
            totalWidth = self.size[0]
        if self.size[1] != -1:
            totalHeight = self.size[1]

        best = wx.Size(totalWidth, totalHeight)
        self.CacheBestSize(best)
        return best

    def on_paint(self, event):
        dc = wx.BufferedPaintDC(self)

        self.draw(dc)

    def draw(self, dc):
        dc.SetFont(self.font)
        dc.SetPen(wx.Pen(self.background_color))
        dc.SetBrush(wx.Brush(self.background_color))

        dc.SetBackground(wx.Brush(self.background_color, wx.SOLID))
        dc.Clear()

        if self.size is None:
            W, H = self.GetSize()
        else:
            if self.size[0] == -1:
                W = self.GetSize().width
            else:
                W = self.size[0]

            if self.size[1] == -1:
                w, h = dc.GetTextExtent(self.text)
                H = self.GetSize().height
            else:
                H = self.size[1]

        # rect = [0, 0, W, H]
        # dc.DrawRectangle(*rect)

        w, h = dc.GetTextExtent(self.text)
        # print(("Text size = ({width}, {height})".format(width=w, height=h)))

        if self.align == "MIDDLE":
            # Draw text in the middle vertical & horizontal
            x_offset = W / 2 - w / 2
            y_offset = H / 2 - h / 2
        elif self.align == wx.TE_LEFT:
            # Draw text on the left but verticaly  centered
            x_offset = 0
            y_offset = H / 2 - h / 2

        dc.SetTextForeground(self.font_color)
        dc.DrawText(self.text, x_offset + self.padding[3], y_offset)
        # self.CacheBestSize(wx.Size(self.size[0], self.size[1]))

    # --------------------------------------------------------------------------
    def on_erase_background(self, evt):
        pass

    # --------------------------------------------------------------------------
    def set_value(self, value):
        self.text = value
        evt = LabelEvent(wx.wxEVT_COMMAND_TEXT_UPDATED, self.GetId())
        evt.SetButtonObj(self)
        evt.SetEventObject(self)
        self.GetEventHandler().ProcessEvent(evt)
        self.Refresh()

    # --------------------------------------------------------------------------
    def change_value(self, value):
        self.text = value
        self.Refresh()

    # --------------------------------------------------------------------------
    def get_value(self):
        return self.text