# -*- coding: utf-8 -*-
from Label import Label
from wx.lib.pubsub import pub
from collections import namedtuple


import wx


Field = namedtuple('Field', 'key label')


###############################################################################
# ComboBox
###############################################################################
class ComboBox(wx.ComboBox):
    def __init__(self, *args, **kwargs):
        super(ComboBox, self).__init__(*args, **kwargs)
        self.Bind(wx.EVT_COMBOBOX, self.on_combobox)

    def on_combobox(self, event):
        self.GetParent().update_value(self.GetValue())
#-----------------------------------------------------------------------------


###############################################################################
# TextField
###############################################################################
class TextField(wx.TextCtrl):
    def __init__(self, parent=None, id=wx.ID_ANY, value='', float_number=True):
        super(TextField, self).__init__(parent, id, value)
        #self._real_number_pattern = re.compile("[-+]?([0-9]*\.[0-9]+|[0-9]+)|[-+]?([0-9]*\.)")

        self.Bind(wx.EVT_LEFT_DOWN, self.on_focus)

        if float_number:
            self.Bind(wx.EVT_CHAR, self.on_char)
        self.Bind(wx.EVT_TEXT, self.on_text)

    def on_char(self, event):
        key_code = event.GetKeyCode()
        print(key_code)
        if key_code in range(48, 58) or key_code in [wx.WXK_BACK, wx.WXK_LEFT, wx.WXK_RIGHT, wx.WXK_DELETE]:
            event.Skip()
        elif key_code == 46:
            value = self.GetValue()
            if value.count(".") == 0:
                event.Skip()
        elif key_code in [43, 45]:
            value = self.GetValue()
            if len(value) == 0 or self.GetInsertionPoint() == 0:
                event.Skip()

    def on_text(self, event):
        event.Skip()
        value = self.GetValue()
        if value == '':
            value = '0.0'
        self.GetParent().update_value(value)

    def on_focus(self, event):
        event.Skip()
        print("on_focus")
        wx.CallAfter(self.SelectAll)
#-----------------------------------------------------------------------------


###############################################################################
# Header
###############################################################################
# wx.Colour(52, 73, 94)
HEADER_COLOR_BG = wx.Colour(50, 61, 67)
HEADER_COLOR_FG = wx.Colour(255, 255, 255)


class Header(wx.Panel):
    def __init__(self, parent, data=None):
        wx.Panel.__init__(self, parent)
        self.data = data
        self.bgcolor = HEADER_COLOR_BG
        self._do_layout()
        self.SetBackgroundColour(HEADER_COLOR_BG)

    def __str__(self):
        return "Header"

    def _do_layout(self):
        sizer = wx.BoxSizer(wx.HORIZONTAL)

        # self.label = wx.StaticText(self, id=wx.ID_ANY, label=self.data['label'])
        self.label = Label(self, text=self.data['label'], color=HEADER_COLOR_FG, align=wx.TE_LEFT,
                                   font_weight=wx.BOLD, padding=(0, 0, 0, 10), font_size=8)
        # self.label.SetForegroundColour(HEADER_COLOR_FG)
        if self.data['type'] == 'TextCtrl':
            try:
                #text = self.data['text']
                self.value = TextField(self, id=wx.ID_ANY, value=self.data['value'],
                                    float_number=False)
            except:
                self.value = TextField(self, id=wx.ID_ANY, value=self.data['value'])
        elif self.data['type'] == 'ComboBox':
            self.value = ComboBox(self, id=wx.ID_ANY, value=self.data['value'],
                                     choices=self.data['choices'], style=wx.CB_READONLY)

        self.eye_btn = wx.Button(self, label='Eye', size=(40, -1))
        self.eye_btn.SetBackgroundColour(wx.Colour(255, 255, 255))
        # self.eye_btn.SetBezelWidth(0)

        sizer.Add(self.eye_btn, 0)
        sizer.Add(self.label, 3, wx.EXPAND)
        if self.data['value'] is not None:
            sizer.Add(self.value, 2, wx.ALIGN_CENTER)

        # self.plus_button = wx.Button(self, id=wx.ID_ANY, label="+")
        # self.plus_button = wx.Button(self, label='+', size=(30, -1))
        # self.plus_button.SetBackgroundColour(self.bgcolor)
        # self.plus_button.SetBezelWidth(0)
        # sizer.Add(self.plus_button, 0)

        # self.delete_button = wx.Button(self, id=wx.ID_ANY, label="x")
        #bmp = wx.Image(GL.img_dir + 'delete.png').ConvertToBitmap()
        self.delete_button = wx.Button(self, label='x', size=(30, -1))
        self.delete_button.SetBackgroundColour(self.bgcolor)
#         self.delete_button.SetBezelWidth(0)

        sizer.Add(self.delete_button, 0)
        self.SetSizerAndFit(sizer)

        self.delete_button.Bind(wx.EVT_BUTTON, self.on_delete)
        # self.plus_button.Bind(wx.EVT_BUTTON, self.on_plus)
        self.eye_btn.Bind(wx.EVT_BUTTON, self.on_eye)

    def update_value(self, value):
        self.data['value'] = value

    def on_eye(self, event):
        event.Skip()
        pub.sendMessage('eye_clicked', data=[self.GetParent().id])

    def on_delete(self, event):
        self.GetParent().destroy()

    def on_plus(self, event):
        self.PopupMenu(self.GetParent().get_popupmenu(self.plus_button))
#-----------------------------------------------------------------------------


###############################################################################
# Row
###############################################################################
# wx.Colour(52, 73, 94)
HEADER_COLOR_BG = wx.Colour(50, 61, 67)
HEADER_COLOR_FG = wx.Colour(255, 255, 255)


class Row(wx.Panel):
    def __init__(self, parent, data=None):
        wx.Panel.__init__(self, parent)
        self.data = data
        self.__do_layout()

    def __do_layout(self):
        sizer = wx.BoxSizer(wx.HORIZONTAL)

        # self.label = wx.StaticText(self, id=wx.ID_ANY, label=self.data['label'])
        self.key = self.data.get('key', None)
        self.label = Label(self, text=self.data['label'], color=wx.Colour(255, 255, 255),
                                   font_size=8, align=wx.TE_LEFT, padding=(0, 0, 0, 20),
                                   bgcolor=self.GetParent().bgcolor)
        if self.data['type'] == 'TextCtrl':
            self.value = TextField(self, id=wx.ID_ANY, value=self.data['value'])
        elif self.data['type'] == 'ComboBox':
            self.value = ComboBox(self, id=wx.ID_ANY, value=self.data['value'],
                                     choices=self.data['choices'], style=wx.CB_READONLY)
        # self.mu = wx.StaticText(self, id=wx.ID_ANY, label=self.data['mu'])
        self.mu = Label(self, text=self.data['mu'], color=wx.Colour(255, 255, 255), font_size=8)

        try:
            enabled = self.data['enabled']
            self.value.Enable(enabled)
        except KeyError:
            self.value.Enable(True)

        sizer.Add(self.label, 3, wx.EXPAND)
        sizer.Add(self.value, 2, wx.ALIGN_CENTER)
        sizer.Add(self.mu, 2, wx.EXPAND)
        self.SetSizerAndFit(sizer)

    def update_value(self, value):
        self.data['value'] = value

    def __str__(self):
        return '{}: {}'.format(self.key, self.value.GetValue())

#-----------------------------------------------------------------------------



