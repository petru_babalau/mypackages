.. FoldingBeam documentation master file, created by
   sphinx-quickstart on Fri Aug 11 16:19:55 2017.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

foldingbeam.FoldingBeam
=======================================
*FoldingBeam* package contains the foldingbeam basic movements
functions.::

FoldingBeam basic functions:

- ``HOME``: home
- ``BENDING``: bending
- ``UP/DOWN``: up/down
- ``LINEAR BENDING``: linear bending


Class Hierarchy
====================================



Known Subclasses
====================================



Method Summary
====================================

================================================================================ ================================================================================
:meth:`~foldingbeam.FoldingBeam.__init__`                                        Default constructor
:meth:`~foldingbeam.FoldingBeam.home`                                            home
:meth:`~foldingbeam.FoldingBeam.bending`                                         bending
:meth:`~foldingbeam.FoldingBeam.up_down`                                         up_down
:meth:`~foldingbeam.FoldingBeam.linear_bending`                                  linear_bending
:meth:`~foldingbeam.FoldingBeam.update_layout`                                   Calling this method immediately repaints the invalidated area of the window.
================================================================================ ================================================================================



Properties Summary
====================================



Class API
====================================



Classes
==========
.. automodule:: foldingbeam
   :members:



.. toctree::
   :maxdepth: 2
   :caption: Contents:



Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`



