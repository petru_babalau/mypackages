# -*- coding: utf-8 -*-
from unittest import TestCase

import packagetest


class TestJoke(TestCase):

    def test_is_string(self):
        s = packagetest.joke()
        self.assertTrue(isinstance(s, str))


class TestAnotherJoke(TestCase):

    def test_is_integer(self):
        i = packagetest.another_joke()
        self.assertTrue(isinstance(i, int))
