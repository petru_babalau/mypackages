# -*- coding: utf-8 -*-
from setuptools import setup


def readme():
    with open('README.rst') as f:
        return f.read()


setup(name='packagetest',
        version='0.1',
        description='Packagetest description',
        long_description=readme(),
        classifiers=[
            'Development Status :: 3 - Alpha',
            'Licence :: OSI Approved :: MIT Licence',
            'Programming Language :: Python :: 3.5',
            'Topic :: Text Processing :: Linguistic',
            ],
        keywords='some words',
        url='https://bitbucket.org/petru_babalau/packagetest',
        author='Petru Babalau',
        author_email='petrubabalau@gmail.com',
        license='MIT',
        packages=['packagetest'],
        install_requires=[
            # 'required_package'
            ],
        include_package_data=True,
        zip_safe=False
    )