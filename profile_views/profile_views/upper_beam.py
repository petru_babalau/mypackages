# -*- coding: utf-8 -*-
import os
import wx
import sys
import wx.lib.newevent as NE


AnimationDoneEvent, EVT_ANIMATION_DONE = NE.NewEvent()
# GooEvent, EVT_GOO = NE.NewCommandEvent()


if hasattr(sys, "frozen"):
    main_dir = os.path.dirname(sys.executable)
    full_real_path = os.path.realpath(sys.executable)
else:
    script_dir = os.path.dirname(__file__)
    main_dir = os.path.dirname(os.path.realpath(sys.argv[0]))
    full_real_path = os.path.realpath(sys.argv[0])


IMG_DIR = os.path.join(main_dir, 'options')


class UpperBeam:

    def __init__(self, scale=1, offset_x=250, offset_y=400, swing_in=False, swing_out=False):
        self.shape = None
        self.scale = scale

        self.swing_in = swing_in
        self.swing_out = swing_out
        self.swing_in_draw = swing_in
        self.swing_out_draw = swing_out

        self.x_ref = offset_x
        self.y_ref = offset_y
        self.height = 0
        self.target_height = self.height
        self.is_opening = False
        self.is_running = False

        self.timer = wx.Timer()
        self.timer.Bind(wx.EVT_TIMER, self._handle_timer)

        self.ref_point = (self.x_ref, self.y_ref)
        self.refresh()

    def set_view(self, view):
        self.view = view

    def _handle_timer(self, event):
        self.open_to(self.target_height, callback=self.timer_done)
        event.Skip()

    def timer_done(self):
        self.timer.Stop()
        self.is_running = False

    def open(self, value, swing_in=True, swing_out=True, is_opening=True):
        self.swing_in = swing_in
        self.swing_out = swing_out
        self.target_height = value
        self.is_opening = is_opening
        self.is_running = True
        self.timer.Start(10)

    def close(self):
        self.open(0, swing_in=True, swing_out=True, is_opening=False)

    def draw(self, dc):
        dc.SetPen(wx.Pen(wx.Colour(37, 37, 37), 2))
        dc.SetBrush(wx.Brush(wx.Colour(47, 47, 47), wx.SOLID))
        dc.DrawPolygon(tuple(self.polygon))

        font = wx.Font(18, wx.ROMAN, wx.ITALIC, wx.NORMAL)
        dc.SetFont(font)

        W, H = self.view.GetClientSize()

        if self.swing_in_draw:
            dc.DrawText("Swing in", 0, 0)
            bmp = wx.Bitmap(os.path.join(IMG_DIR, 'swing_in_inactive.png'), wx.BITMAP_TYPE_PNG)
            w, h = bmp.GetSize()
            dc.DrawBitmap(bmp, W - w - 5, 5, True)
        if self.swing_out_draw and not self.swing_in:
            dc.DrawText("Swing out", 0, 0)
            if self.is_opening:
                bmp = wx.Bitmap(os.path.join(IMG_DIR, 'swing_in_out.png'), wx.BITMAP_TYPE_PNG)
            else:
                bmp = wx.Bitmap(os.path.join(IMG_DIR, 'cp_working_position.png'),
                                wx.BITMAP_TYPE_PNG)
            w, h = bmp.GetSize()
            dc.DrawBitmap(bmp, W - w - 5, 5, True)

        font = wx.Font(10, wx.ROMAN, wx.ITALIC, wx.NORMAL)
        dc.SetFont(font)
        text = "UB HEIGHT: {}mm".format(self.height)
        dc.DrawText(text, W - 50 - 180, 5)

    def set_scale(self, value):
        self.scale = value
        self.ref_point = (self.ref_point[0], self.y_ref - self.height * self.scale)
        self.refresh()

    def refresh(self):
        self.polygon = [self.ref_point,
                        (self.ref_point[0] + 95.0 * self.scale,
                            self.ref_point[1] + 0 * self.scale),
                        (self.ref_point[0] + 95.0 * self.scale,
                            self.ref_point[1] - 2.0 * self.scale),
                        (self.ref_point[0] + 83.0 * self.scale,
                            self.ref_point[1] - 12.0 * self.scale),
                        (self.ref_point[0] + 83.0 * self.scale,
                            self.ref_point[1] - 30.0 * self.scale),
                        (self.ref_point[0] + 124.0 * self.scale,
                            self.ref_point[1] - 128.0 * self.scale),
                        (self.ref_point[0] + 124.0 * self.scale,
                            self.ref_point[1] - 177.0 * self.scale),
                        (self.ref_point[0] + 144.0 * self.scale,
                            self.ref_point[1] - 177.0 * self.scale),
                        (self.ref_point[0] + 155.0 * self.scale,
                            self.ref_point[1] - 200.0 * self.scale),
                        (self.ref_point[0] + 155.0 * self.scale,
                            self.ref_point[1] - 211.0 * self.scale),
                        (self.ref_point[0] + 163.0 * self.scale,
                            self.ref_point[1] - 211.0 * self.scale),
                        (self.ref_point[0] + 163.0 * self.scale,
                            self.ref_point[1] - 219.0 * self.scale),
                        (self.ref_point[0] + 73.0 * self.scale,
                            self.ref_point[1] - 219.0 * self.scale),
                        (self.ref_point[0] + 73.0 * self.scale,
                            self.ref_point[1] - 227.0 * self.scale),
                        (self.ref_point[0] + 78.0 * self.scale,
                            self.ref_point[1] - 227.0 * self.scale),
                        (self.ref_point[0] + 78.0 * self.scale,
                            self.ref_point[1] - 236.0 * self.scale),
                        (self.ref_point[0] + 60.0 * self.scale,
                            self.ref_point[1] - 232.0 * self.scale),
                        (self.ref_point[0] + 60.0 * self.scale,
                            self.ref_point[1] - 128.0 * self.scale),
                        (self.ref_point[0] + 50.0 * self.scale,
                            self.ref_point[1] - 22.0 * self.scale),
                        (self.ref_point[0] + 4.0 * self.scale,
                            self.ref_point[1] - 5.0 * self.scale),
                        ]

    def open_to(self, value, callback=None):
        step = 1
        value = int(value)

        self.swing_in_draw = self.swing_in
        self.swing_out_draw = self.swing_out

        if value > self.height:
            self.ref_point = (self.ref_point[0], self.ref_point[1] - step * self.scale)
            self.height += step
            self.refresh()
        elif value < self.height:
            self.ref_point = (self.ref_point[0], self.ref_point[1] + step * self.scale)
            self.height -= step
            self.refresh()
        else:
            self.height = value
            # self.swing_in_draw = False
            if self.swing_out:
                self.swing_in = False
                self.swing_in_draw = False
            self.swing_out_draw = self.swing_out
            callback()
        self.view.Refresh()

