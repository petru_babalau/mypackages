# -*- coding: utf-8 -*-
import wx
import math


class Action:
    fb_open = 'open'
    fb_rotate = 'rotate'


def rotate(shape, angle=180):
    angle = math.radians(angle)

    def _rot(x, y):
        _x = math.cos(angle) * x - math.sin(angle) * y
        _y = math.sin(angle) * x + math.cos(angle) * y
        return (_x, _y)

    shape = [_rot(x, y) for x, y in shape]
    return shape


def translate(shape, dist=-14):
    shape = [(x + dist, y) for x, y in shape]
    return shape


SHAPE = [(0, 0), (0, 23), (34, 42), (34, 425), (-54, 425), (-54, 107),
         (14, 107), (14, 54), (- 14, 28), (-14, 0), (0, 0)]

SHAPE_INV = translate(rotate(SHAPE))


class FoldingBeam:

    def __init__(self, scale=1, offset_x=250, offset_y=400, home=True, height=0, angle=0):
        self.shape = None
        self.scale = scale

        self.x_ref = offset_x
        self.y_ref = offset_y

        self.height = height
        self.target_height = height
        self._theta_fb = angle
        self.target_angle = angle
        self.home = home

        self.is_running = False
        self.action = None
        self.timer = wx.Timer()
        self.timer.Bind(wx.EVT_TIMER, self._handle_timer)

        self.ref_point = (self.x_ref, self.y_ref)
        self.refresh()

    def set_view(self, view):
        self.view = view

    def _handle_timer(self, event):
        if self.action == Action.fb_open:
            self.open_to(self.target_height, callback=self.timer_done)
        elif self.action == Action.fb_rotate:
            self.rotate_to(self.target_angle, callback=self.timer_done)
        event.Skip()

    def timer_done(self):
        self.timer.Stop()
        self.is_running = False

    def open(self, value):
        self.action = Action.fb_open
        self.target_height = value
        self.timer.Start(10)
        self.is_running = True

    def rotate(self, value):
        self.action = Action.fb_rotate
        self.target_angle = value
        self.timer.Start(10)
        self.is_running = True

    def home_down(self):
        self.action = Action.fb_rotate
        self.target_angle = 0
        self.timer.Start(10)
        self.is_running = True

    def home_up(self):
        self.action = Action.fb_rotate
        self.target_angle = 180
        self.timer.Start(10)
        self.is_running = True

    def draw(self, dc):
        W, H = self.view.GetClientSize()

        dc.SetPen(wx.Pen(wx.Colour(37, 37, 37), 2))
        dc.SetBrush(wx.Brush(wx.Colour(47, 47, 47), wx.SOLID))
        dc.DrawPolygon(tuple(self.first_part))

        font = wx.Font(10, wx.ROMAN, wx.ITALIC, wx.NORMAL)
        dc.SetFont(font)
        text = "FB HEIGHT: {}mm".format(self.height)
        dc.DrawText(text, W - 50 - 180, 20)
        text = "FB HEIGHT: {}deg".format(self._theta_fb)
        dc.DrawText(text, W - 50 - 180, 35)

    def set_scale(self, value):
        self.scale = value
        self.ref_point = (self.ref_point[0], self.y_ref + self.height * self.scale)
        self.refresh()

    def refresh(self):
        angle = math.radians(self._theta_fb)

        def _rot(x, y):
            _x = self.x_ref + math.cos(angle) * (x - self.x_ref) -\
                 math.sin(angle) * (y - self.y_ref)
            _y = self.y_ref + math.sin(angle) * (x - self.x_ref) +\
                 math.cos(angle) * (y - self.y_ref)
            return (_x, _y)

        shape = SHAPE
        if not self.home:
            shape = translate(SHAPE, dist=14)
        self.first_part = [_rot(self.ref_point[0] + x * self.scale,
                                    self.ref_point[1] + y * self.scale) for x, y in shape]

    def open_to(self, value, callback=None):
        step = 1
        value = int(value)
        if value > self.height:
            self.ref_point = (self.ref_point[0], self.ref_point[1] + step * self.scale)
            self.height += step
            self.refresh()
        elif value < self.height:
            self.ref_point = (self.ref_point[0], self.ref_point[1] - step * self.scale)
            self.height -= step
            self.refresh()
        else:
            self.height = value
            callback()
        self.view.Refresh()

    def rotate_to(self, value, callback=None, view=None):
        step = 1
        if value > self._theta_fb:
            self._theta_fb += step
            self.refresh()
        elif value < self._theta_fb:
            self._theta_fb -= step
            self.refresh()
        else:
            callback()
        self.view.Refresh()

'''
        dc.SetBrush(wx.Brush(wx.Colour(230, 230, 230), wx.SOLID))
        dc.SetPen(wx.Pen(wx.Colour(37, 37, 37), 2))

        dc.DrawPolygon((_rot(x_ref2, y_ref2),
                        _rot(x_ref2 + 100 * scale, y_ref2),
                        _rot(x_ref2 + 100 * scale, y_ref2 + 230),
                        _rot(x_ref2, y_ref2 + 230)
            ))
'''