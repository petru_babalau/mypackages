# -*- coding: utf-8 -*-
import wx
import copy
from lower_beam import LowerBeam
from upper_beam import UpperBeam
from folding_beam import FoldingBeam
from manipulator import Manipulator


ANIMATIONS = [['upper_beam', 'open', 50],
                # ['manipulator', 'open', 30],
                ['manipulator', 'move_abs', 250],
                # ['manipulator', 'open', 0],
                # ['manipulator', 'move_abs', 125],
                ['upper_beam', 'open', 0],
                ['folding_beam', 'rotate', 90],
                ['folding_beam', 'open', 50],
                ['folding_beam', 'rotate', 0],
                ['folding_beam', 'open', 0]]
            #self.view.upper_beam.open(0)
            #self.view.folding_beam.rotate(90)
            #self.view.folding_beam.open(50)]


class View(wx.Control):

    def __init__(self, *args, **kwargs):
        wx.Control.__init__(self, *args, **kwargs)
        self.SetBackgroundColour(wx.Colour(0, 103, 180))

        self._scale = 1
        self.current_action_id = None
        self.manip_upper_part_width = 30
        self.manip_upper_part_length = 100
        self._h_translate = 0.0
        self._x = 150
        self._y = 200
        self._angle = 0.0
        self._thickness = 1.2
        self._manipulator_length = 150
        self._manipulator_width = 10
        self.active_side = 1

        self.is_ready = True

        self.ub_height_up = 2.0

        self.Bind(wx.EVT_PAINT, self.on_paint)
        self.Bind(wx.EVT_ERASE_BACKGROUND, self.on_erase_background)

        wx.CallLater(200, self.set_sizes)

        self.lower_beam = LowerBeam()
        self.upper_beam = UpperBeam()
        self.upper_beam.set_view(self)
        self.folding_beam = FoldingBeam(angle=0, home=True)
        self.folding_beam.set_view(self)

        self.manipulator = Manipulator()
        self.manipulator.set_view(self)

    def on_paint(self, event):
        dc = wx.PaintDC(self)
        dc.Clear()
        self.draw(dc)

    def set_sizes(self):
        self._width, self._height = self.GetClientSize()
        self._center = (self._width / 2, self._height / 2)

    def on_erase_background(self, event):
        pass

    def draw(self, dc):
        self.lower_beam.draw(dc)
        self.upper_beam.draw(dc)
        self.folding_beam.draw(dc)
        self.manipulator.draw(dc)

    def increase_scale(self, factor=0.1):
        if self._scale < 10:
            self._scale += factor
            self.lower_beam.set_scale(self._scale)
            self.upper_beam.set_scale(self._scale)
            self.folding_beam.set_scale(self._scale)
            self.manipulator.set_scale(self._scale)
            self.Refresh()

    def reduce_scale(self, factor=0.1):
        if self._scale > 0.2:
            self._scale -= factor
            self.lower_beam.set_scale(self._scale)
            self.upper_beam.set_scale(self._scale)
            self.folding_beam.set_scale(self._scale)
            self.manipulator.set_scale(self._scale)
            self.Refresh()

    # Manipulator
    def open_manipulator(self, value):
        self.manipulator.open(value)

    def close_manipulator(self):
        self.manipulator.close()

    # Upper beam
    def open_upper_beam(self, value):
        self.upper_beam.open(value)

    def close_upper_beam(self):
        self.upper_beam.close()

    # Folding beam
    def open_folding_beam(self, value):
        self.folding_beam.open(value)

    def rotate_folding_beam(self, value):
        self.folding_beam.rotate(value)

    # FB home
    def home_fb(self):
        self.folding_beam.home_down()

    def home_up_fb(self):
        self.folding_beam.home_up()


class LateralView(wx.Panel):

    def __init__(self, parent):
        wx.Panel.__init__(self, parent)
        self.SetBackgroundColour((103, 103, 103))

        self.queue = copy.deepcopy(ANIMATIONS)

        # self.program = program
        self.running_action = None

        self.view = View(parent=self, style=wx.DOUBLE_BORDER)
        self.controls = wx.Panel(self, style=wx.DOUBLE_BORDER)
        sizer = wx.BoxSizer(wx.VERTICAL)

        self.controls.plus_btn = wx.Button(self.controls, label='+', size=(30, 30))
        self.controls.plus_btn.SetBackgroundColour(wx.Colour(255, 255, 255))

        self.controls.minus_btn = wx.Button(self.controls, label='-', size=(30, 30))
        self.controls.minus_btn.SetBackgroundColour(wx.Colour(255, 255, 255))

        # Folding beam controls
        self.controls.open_btn = wx.Button(self.controls, label='O', size=(30, 30))
        self.controls.open_btn.SetBackgroundColour(wx.Colour(255, 255, 255))

        self.controls.rotate_btn = wx.Button(self.controls, label='R', size=(30, 30))
        self.controls.rotate_btn.SetBackgroundColour(wx.Colour(255, 255, 255))

        self.controls.home_btn = wx.Button(self.controls, label='H', size=(30, 30))
        self.controls.home_btn.SetBackgroundColour(wx.Colour(255, 255, 255))

        self.controls.home_up_btn = wx.Button(self.controls, label='!H', size=(30, 30))
        self.controls.home_up_btn.SetBackgroundColour(wx.Colour(255, 255, 255))

        sizer.Add(self.controls.plus_btn, 0)
        sizer.Add(self.controls.minus_btn, 0)
        sizer.Add(self.controls.open_btn, 0)
        sizer.Add(self.controls.rotate_btn, 0)
        sizer.Add(self.controls.home_btn, 0)
        sizer.Add(self.controls.home_up_btn, 0)

        self.controls.open_btn.Bind(wx.EVT_BUTTON, self.on_open_btn)
        self.controls.rotate_btn.Bind(wx.EVT_BUTTON, self.on_rotate_btn)
        self.controls.home_btn.Bind(wx.EVT_BUTTON, self.on_home_btn)
        self.controls.home_up_btn.Bind(wx.EVT_BUTTON, self.on_home_up_btn)

        sizer.Add(wx.StaticLine(self.controls, wx.LI_HORIZONTAL), 0,
                                wx.EXPAND | wx.TOP | wx.BOTTOM, 5)

        # Upper beam controls
        self.controls.ub_open_btn = wx.Button(self.controls, label='O', size=(30, 30))
        self.controls.ub_open_btn.SetBackgroundColour(wx.Colour(255, 255, 255))
        self.controls.ub_close_btn = wx.Button(self.controls, label='C', size=(30, 30))
        self.controls.ub_close_btn.SetBackgroundColour(wx.Colour(255, 255, 255))

        sizer.Add(self.controls.ub_open_btn, 0)
        sizer.Add(self.controls.ub_close_btn, 0)

        self.controls.ub_open_btn.Bind(wx.EVT_BUTTON, self.on_ub_open_btn)
        self.controls.ub_close_btn.Bind(wx.EVT_BUTTON, self.on_ub_close_btn)

        sizer.Add(wx.StaticLine(self.controls, wx.LI_HORIZONTAL), 0,
                                wx.EXPAND | wx.TOP | wx.BOTTOM, 5)

        # Manipulator
        self.controls.manip_open_btn = wx.Button(self.controls, label='O', size=(30, 30))
        self.controls.manip_open_btn.SetBackgroundColour(wx.Colour(255, 255, 255))
        self.controls.manip_close_btn = wx.Button(self.controls, label='C', size=(30, 30))
        self.controls.manip_close_btn.SetBackgroundColour(wx.Colour(255, 255, 255))
        self.controls.manip_move_to_btn = wx.Button(self.controls, label='A', size=(30, 30))
        self.controls.manip_move_to_btn.SetBackgroundColour(wx.Colour(255, 255, 255))

        sizer.Add(self.controls.manip_open_btn, 0)
        sizer.Add(self.controls.manip_close_btn, 0)
        sizer.Add(self.controls.manip_move_to_btn, 0)

        self.controls.manip_open_btn.Bind(wx.EVT_BUTTON, self.on_manip_open_btn)
        self.controls.manip_close_btn.Bind(wx.EVT_BUTTON, self.on_manip_close_btn)
        self.controls.manip_move_to_btn.Bind(wx.EVT_BUTTON, self.on_manip_move_to_btn)

        sizer.Add(wx.StaticLine(self.controls, wx.LI_HORIZONTAL), 0,
                                wx.EXPAND | wx.TOP | wx.BOTTOM, 5)

        # Cycle
        self.controls.cycle_run_btn = wx.Button(self.controls, label='R', size=(30, 30))
        self.controls.cycle_run_btn.SetBackgroundColour(wx.Colour(255, 255, 255))

        self.controls.cycle_run_btn.Bind(wx.EVT_BUTTON, self.on_cycle_run_btn)
        sizer.Add(self.controls.cycle_run_btn, 0)

        self.controls.SetSizerAndFit(sizer)

        topview_sizer = wx.BoxSizer(wx.HORIZONTAL)
        topview_sizer.Add(self.view, 8, wx.EXPAND)
        topview_sizer.Add(self.controls, 0, wx.EXPAND)
        self.SetSizerAndFit(topview_sizer)

        self.controls.plus_btn.Bind(wx.EVT_BUTTON, self.on_plus_btn)
        self.controls.minus_btn.Bind(wx.EVT_BUTTON, self.on_minus_btn)

        self.cycle_timer = wx.Timer()
        self.cycle_timer.Bind(wx.EVT_TIMER, self._handle_cycle_timer)

    def on_plus_btn(self, event):
        self.view.increase_scale()
        event.Skip()

    def on_minus_btn(self, event):
        self.view.reduce_scale()
        event.Skip()

    def on_manip_open_btn(self, event):
        self.view.open_manipulator(50)
        event.Skip()

    def on_manip_move_to_btn(self, event):
        choices = range(0, 310, 10)
        import random
        value = random.choice(choices)
        self.view.manipulator.move_abs(value)
        event.Skip()

    def on_manip_close_btn(self, event):
        self.view.close_manipulator()
        event.Skip()

    def on_ub_open_btn(self, event):
        self.view.open_upper_beam(50)
        event.Skip()

    def on_ub_close_btn(self, event):
        self.view.close_upper_beam()
        event.Skip()

    def on_home_btn(self, event):
        self.view.home_fb()
        event.Skip()

    def on_home_up_btn(self, event):
        self.view.home_up_fb()
        event.Skip()

    def on_open_btn(self, event):
        '''
        import random
        choices = range(0, 91, 10)
        ch = random.choice(choices)
        print("Open to", ch)
        self.run_cycle(ch)
        '''
        self.view.open_folding_beam(50)
        event.Skip()

    def on_rotate_btn(self, event):
        '''
        import random
        choices = range(0, 91, 10)
        ch = random.choice(choices)
        print("Open to", ch)
        self.run_cycle(ch)
        '''
        self.view.rotate_folding_beam(100)
        event.Skip()

    def on_cycle_run_btn(self, event):
        self.cycle_timer.Start(300)
        self.queue = copy.deepcopy(ANIMATIONS)
        event.Skip()

    def cycle_run(self, callback=None):
        print("cycle_run")
        if self.running_action is None or not getattr(self.running_action, 'is_running'):
            item = self.queue.pop(0)
            obj = getattr(self.view, item[0])
            self.running_action = obj
            getattr(obj, item[1])(item[2])
            print(obj, item[1], item[2])
        if not self.queue:
            self.running_action = None
            callback()

    def _handle_cycle_timer(self, event):
        self.cycle_run(callback=self.timer_done)
        event.Skip()

    def timer_done(self):
        self.cycle_timer.Stop()
        print("Cycle ended.")


if __name__ == '__main__':
    app = wx.App()

    frame = wx.Frame(None)
    view = LateralView(frame)
    frame.Maximize(True)
    frame.Show()

    app.MainLoop()