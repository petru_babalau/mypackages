# -*- coding: utf-8 -*-
import wx


class LowerBeam:

    def __init__(self, scale=1, offset_x=250, offset_y=400):
        self.shape = None
        self.scale = scale

        self.x_ref = offset_x
        self.y_ref = offset_y

        self.ref_point = (self.x_ref, self.y_ref)
        self.first_part = [self.ref_point,
                            (self.ref_point[0] + 90.0 * self.scale,
                                self.ref_point[1] + 0 * self.scale),
                            (self.ref_point[0] + 90.0 * self.scale,
                                self.ref_point[1] + 37 * self.scale),
                            (self.ref_point[0] + 50.0 * self.scale,
                                self.ref_point[1] + 37 * self.scale),
                            (self.ref_point[0] + 50.0 * self.scale,
                                self.ref_point[1] + 35 * self.scale),
                            (self.ref_point[0] + 60.0 * self.scale,
                                self.ref_point[1] + 35 * self.scale),
                            (self.ref_point[0] + 60.0 * self.scale,
                                self.ref_point[1] + 22 * self.scale)
                        ]

        self.ref_point_2 = (self.ref_point[0] + 90.0 * self.scale,
                                self.ref_point[1] + 0 * self.scale)

        self.second_part = [self.ref_point_2,
                            (self.ref_point[0] + 2050.0 * self.scale,
                                self.ref_point[1] + 0 * self.scale),
                            (self.ref_point[0] + 2050.0 * self.scale,
                                self.ref_point[1] + 900 * self.scale),
                            (self.ref_point[0] + 50.0 * self.scale,
                                self.ref_point[1] + 900 * self.scale),
                            (self.ref_point[0] + 50.0 * self.scale,
                                self.ref_point[1] + 38.0 * self.scale),
                            (self.ref_point[0] + 90.0 * self.scale,
                                self.ref_point[1] + 38.0 * self.scale),
                            ]

    def draw(self, dc):
        dc.SetPen(wx.Pen(wx.Colour(37, 37, 37), 2))
        dc.SetBrush(wx.Brush(wx.Colour(47, 47, 47), wx.SOLID))
        dc.DrawPolygon(tuple(self.first_part))

        dc.SetBrush(wx.Brush(wx.Colour(240, 240, 240), wx.SOLID))
        dc.DrawPolygon(tuple(self.second_part))

    def set_scale(self, value):
        self.scale = value
        self.scale_lower_beam()

    def scale_lower_beam(self):
        self.first_part = [self.ref_point,
                            (self.ref_point[0] + 90.0 * self.scale,
                                self.ref_point[1] + 0 * self.scale),
                            (self.ref_point[0] + 90.0 * self.scale,
                                self.ref_point[1] + 37 * self.scale),
                            (self.ref_point[0] + 50.0 * self.scale,
                                self.ref_point[1] + 37 * self.scale),
                            (self.ref_point[0] + 50.0 * self.scale,
                                self.ref_point[1] + 35 * self.scale),
                            (self.ref_point[0] + 60.0 * self.scale,
                                self.ref_point[1] + 35 * self.scale),
                            (self.ref_point[0] + 60.0 * self.scale,
                                self.ref_point[1] + 22 * self.scale)
                        ]

        self.ref_point_2 = (self.ref_point[0] + 90.0 * self.scale,
                                self.ref_point[1] + 0 * self.scale)

        self.second_part = [self.ref_point_2,
                            (self.ref_point[0] + 2050.0 * self.scale,
                                self.ref_point[1] + 0 * self.scale),
                            (self.ref_point[0] + 2050.0 * self.scale,
                                self.ref_point[1] + 900 * self.scale),
                            (self.ref_point[0] + 50.0 * self.scale,
                                self.ref_point[1] + 900 * self.scale),
                            (self.ref_point[0] + 50.0 * self.scale,
                                self.ref_point[1] + 38.0 * self.scale),
                            (self.ref_point[0] + 90.0 * self.scale,
                                self.ref_point[1] + 38.0 * self.scale),
                            ]

