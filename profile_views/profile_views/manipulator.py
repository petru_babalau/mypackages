# -*- coding: utf-8 -*-
import wx


class Action:
    manip_open = 'open'
    manip_rotate = 'rotate'
    manip_move = 'move'


def translate(shape, dist=-14):
    shape = [(x + dist, y) for x, y in shape]
    return shape


L, W, H = (150, 10, 20)
BODY = [[0, 0, 0], [0, 0, H], [0, L, H], [0, L, 0],
        [W, 0, 0], [W, 0, H], [W, L, H], [W, L, 0]]


class Manipulator:

    def __init__(self, scale=1, offset_x=250, offset_y=400):
        self.shape = None
        self.scale = scale

        self.x_ref = offset_x
        self.y_ref = offset_y
        self.height = 0
        self.target_height = self.height
        self.position = 0
        self.target_position = self.position
        self.action = None
        self.is_running = False

        self.timer = wx.Timer()
        self.timer.Bind(wx.EVT_TIMER, self._handle_timer)

        self.ref_point = (self.x_ref, self.y_ref)
        self.refresh()

    def set_view(self, view):
        self.view = view

    def _handle_timer(self, event):
        if self.action == Action.manip_open:
            self.open_to(self.target_height, callback=self.timer_done)
        elif self.action == Action.manip_move:
            self.move_to(self.target_position, callback=self.timer_done)
        event.Skip()

    def timer_done(self):
        self.timer.Stop()
        self.is_running = False

    def move_abs(self, value):
        self.action = Action.manip_move
        self.target_position = int(value)
        self.timer.Start(5)
        self.is_running = True

    def open(self, value):
        self.action = Action.manip_open
        self.target_height = value
        self.timer.Start(5)
        self.is_running = True

    def close(self):
        self.open(0)

    def draw(self, dc):
        W, H = self.view.GetClientSize()

        dc.SetPen(wx.Pen(wx.Colour(37, 37, 37), 2))
        dc.SetBrush(wx.Brush(wx.Colour(47, 47, 47), wx.SOLID))
        dc.DrawPolygon(tuple(self.polygon))

        font = wx.Font(10, wx.ROMAN, wx.ITALIC, wx.NORMAL)
        dc.SetFont(font)
        text = "MANIPULATOR DISTANCE: {}mm".format(self.position)
        dc.DrawText(text, W - 500, 5)

    def set_scale(self, value):
        self.scale = value
        self.ref_point = (self.ref_point[0], self.y_ref - self.height * self.scale)
        self.refresh()

    def refresh(self):
        self.polygon = [(self.ref_point[0] + y * self.scale, self.ref_point[1] - z * self.scale)
                                                for x, y, z in BODY if x == W]
        self.polygon = translate(self.polygon, dist=-L/2)

    def open_to(self, value, callback=None):
        step = 1
        value = int(value)
        if value > self.height:
            self.ref_point = (self.ref_point[0], self.ref_point[1] - step * self.scale)
            self.height += step
            self.refresh()
        elif value < self.height:
            self.ref_point = (self.ref_point[0], self.ref_point[1] + step * self.scale)
            self.height -= step
            self.refresh()
        else:
            self.height = value
            callback()
        self.view.Refresh()

    def move_to(self, value, callback=None):
        step = 1
        value = int(value)
        if value > self.position:
            self.ref_point = (self.ref_point[0] + step * self.scale, self.ref_point[1])
            self.position += step
            self.refresh()
        elif value < self.position:
            self.ref_point = (self.ref_point[0] - step * self.scale, self.ref_point[1])
            self.position -= step
            self.refresh()
        else:
            self.position = value
            callback()
        self.view.Refresh()


