# -*- coding: utf-8 -*-
import wx
import action
from utils import Header, Row
from utils import Field


#-------------------------------------------------------------------------
# Manipulator POSITION/ROTATE
ROTATION_POS = ('00000080', 'xx000000')
ROTATION_ANGLE = ('00000080', 'xx000000')
POSITION = ('00000080', 'xx000000')
REDUCED_ROTATIONAL_SPEED = ('20', '44')
REDUCED_LINEAR_SPEED = ('20', '45')
REQUEST_FOR_NEW_BLANK = ('20', '43')

# Manipulator LOAD
LOADING_POSITION = ('00000080', 'xx000000')
BLANK_DEF = ('00000080', 'xx000000')

# Manipulator UNLOAD
ROTATION_POS = ('00000080', 'xx000000')
ROT_ANGLE = ('00000080', 'xx000000')
UNLOAD_POSITION = ('00000080', 'xx000000')
UNLODING_BELT = ('20', '43')

# Manipulator REPOSITION
PULL_BACK = ('00000080', 'xx000000')
UPPER_BEAM_DOWN = ('00000080', 'xx000000')
OFFSET = ('00000080', 'xx000000')
UPPER_BEAM_UP = '00'
HALF_UP = ('20', '43')
#-----------------------------------------------------------------------------


###############################################################################
# Actions
###############################################################################
class Actions:
    """ Actions """
    position_rotate = Field(key='position_rotate', label='POSITION ROTATE')
    load = Field(key='load', label='LOAD')
    unload = Field(key='unload', label='UNLOAD')
    reposition = Field(key='reposition', label='REPOSITION')
#-----------------------------------------------------------------------------


HEX_STR = {
    Actions.position_rotate.key: '02' + '00' * 9 +
                                 '04' + '00' * 9 +
                                 '00000080' * 7 +
                                 '{rotation_pos}'
                                 '{rotation_angle}'
                                 '{position}'
                                 '00000080' * 3 +
                                 '2020'
                                 '{request_for_new_blank}'
                                 '{reduced_rotational_speed}'
                                 '{reduced_linear_speed}'
                                 '20' * 29 +
                                 '00000080' * 4 +
                                 '20' * 8,
    Actions.load.key: '02' + '00' * 9 +
                      '05' + '00' * 9 +
                      '00000080' * 9 +
                      '{loading_position}'
                      '00000080' * 3 +
                      '20' * 34 +
                      '00000080' * 2 +
                      '{blank_def}'
                      '00000080'
                      '20' * 8,
    Actions.unload.key: '02' + '00' * 9 +
                        '06' + '00' * 9 +
                        '00000080' * 7 +
                        '{rotation_pos}'
                        '{unload_position}'
                        '{unload_position}'
                        '00000080' * 3 +
                        '2020'
                        '{unloding_belt}'
                        '20' * 31 +
                        '00000080' * 4 +
                        '20' * 8,
    Actions.reposition.key: '02' + '00' * 9 +
                            '07' + '00' * 9 +
                            '00000080'
                            '{upper_beam_down}'
                            '{upper_beam_up}'
                            '00000080' * 6 +
                            '{pull_back}'
                            '{offset}000080'    # ?????
                            '00000080' * 2 +
                            '2020'
                            '{half_up}'
                            '20' * 31 +
                            '00000080' * 4 +
                            '20' * 8
}


###############################################################################
# Variables
###############################################################################
class Variables:
    """ Variables """
    rotation_position = Field(key='rotation_position', label='ROTATION POSITIONS')
    rotation_angle = Field(key='rotation_angle', label='ROTATION ANGLE')
    position = Field(key='position', label='POSITION')
    loading_position = Field(key='loading_position', label='LOADING POSITION')
    blank_def = Field(key='blank_def', label='BLANK DEF(BLK)')
    unload_position = Field(key='unload_position', label='UNLOAD POSITION')
    pull_back = Field(key='pull_back', label='PULL BACK')
    upper_beam_down = Field(key='upper_beam_down', label='UPPER-BEAM DOWN')
    offset = Field(key='offset', label='OFFSET')
    upper_beam_up = Field(key='upper_beam_up', label='UPPER-BEAM UP')
    reduced_rotational_speed = Field(key='reduced_rotational_speed',
                                        label='REDUCED_ROTATIONAL_SPEED')
    reduced_linear_speed = Field(key='reduced_linear_speed', label='REDUCED_LINEAR_SPEED')
    request_new_blank = Field(key='request_new_blank', label='REQUEST_FOR_NEW_BLANK')
    unloading_belt = Field(key='unloading_belt', label='UNLOADING_BELT')
    half_up = Field(key='half_up', label='HALF_UP')
#-----------------------------------------------------------------------------


FIELDS = {
    Actions.position_rotate.key: [Variables.rotation_position.key,
                                     Variables.rotation_angle.key,
                                     Variables.position.key,
                                     Variables.reduced_rotational_speed.key,
                                     Variables.reduced_linear_speed.key,
                                     Variables.request_new_blank.key],
    Actions.load.key: [Variables.loading_position.key,
                        Variables.blank_def.key],
    Actions.unload.key: [Variables.rotation_position.key,
                         Variables.rotation_angle.key,
                         Variables.unload_position.key,
                         Variables.unloading_belt.key],
    Actions.reposition.key: [Variables.pull_back.key,
                             Variables.upper_beam_down.key,
                             Variables.offset.key,
                             Variables.upper_beam_up.key,
                             Variables.half_up.key]
}


DEFAULT_VALUES = {
    Actions.position_rotate.key: {Variables.rotation_position.key: '0.0',
                                     Variables.rotation_angle.key: '0.0',
                                     Variables.position.key: '0.0',
                                     Variables.reduced_rotational_speed.key: 'DEACTIVATED',
                                     Variables.reduced_linear_speed.key: 'DEACTIVATED',
                                     Variables.request_new_blank.key: 'DEACTIVATED'},
    Actions.load.key: {Variables.loading_position.key: '0.0',
                         Variables.blank_def.key: '0.0'},
    Actions.unload.key: {Variables.rotation_position.key: '0.0',
                         Variables.rotation_angle.key: '0.0',
                         Variables.unload_position.key: '0.0',
                         Variables.unloading_belt.key: 'DEACTIVATED'},
    Actions.reposition.key: {Variables.pull_back.key: '0.0',
                             Variables.upper_beam_down.key: '0.0',
                             Variables.offset.key: '0.0',
                             Variables.upper_beam_up.key: '0.0',
                             Variables.half_up.key: 'DEACTIVATED'}
}


OPTIONS = {
    Actions.position_rotate.key: {'reduced_rotational_speed': False,
                                 'reduced_linear_speed': False,
                                 'request_for_new_blank': False},
    Actions.load.key: {},
    Actions.unload.key: {'unloading_belt': False},
    Actions.reposition.key: {'half_up': False}
}

# TODO:
HEX_VALUES = {}


###############################################################################
# ManipulatorModel
###############################################################################
class ManipulatorModel:
    """ Manipulator model class"""
    actions = [Actions.position_rotate.key,
               Actions.load.key,
               Actions.unload.key,
               Actions.reposition.key]

    def __init__(self, *args, **kwargs):
        action = kwargs.get('action', self.actions[0])
        self.hex_str = ''
        self.init_action(action)

    def __str__(self):
        string = '<Manipulator>'
        for attr in self.__dict__:
            string += '\n'
            string += '\t' + attr + ' = ' + str(getattr(self, attr))
        string += '\n</Manipulator>'
        return string

    def init_action(self, action):
        """ Init hex string & set attributes. """
        self.action = action
        self.hex_str = HEX_STR[action]
        for item in DEFAULT_VALUES[action].items():
            setattr(self, item[0], item[1])

    def formated_hex_str(self):
        """ Returns a formated hex string. """
        data = {}
        for attr in FIELDS[self.action]:
            data[attr] = HEX_VALUES[attr]
        formated_hex_str = self.hex_str.format(**data)
        return formated_hex_str

    def change_action(self, action=None):
        """ Change the main action. """
        self.delete_attributes()
        self.set_hex_str(action)
        self.action = action
        self.init_action(action)

    def set_hex_str(self, action):
        """ Set hex string """
        self.hex_str = HEX_STR[action]

    def delete_attributes(self):
        """ Delete the attributes of the model. """
        for attr in FIELDS[self.action]:
            delattr(self, attr)
#-----------------------------------------------------------------------------


def run():
    manip = ManipulatorModel()
    print(manip)
    manip.change_action(Actions.load.key)
    print(manip)
    manip.change_action(Actions.unload.key)
    print(manip)
    manip.change_action(Actions.reposition.key)
    print(manip)
    manip.change_action(Actions.position_rotate.key)
    print(manip)
    manip.delete_attributes()
    print(manip)



###############################################################################
# Manipulator: Basic functions
#
# 0: Position and rotate main manipulator
# 1: Load main manipulator
# 2: Unload main manipulator
# 3: Reposition main manipulator
#
###############################################################################

POSITION_ROTATE = 0
LOAD = 1
UNLOAD = 2
REPOSITION = 3

BASIC_FUNC_LABELS = {
    0: 'POSITION ROTATE',
    1: 'LOAD',
    2: 'UNLOAD',
    3: 'REPOSITION'
}
INV_BASIC_FUNC_LABELS = {v: k for k, v in BASIC_FUNC_LABELS.items()}


class Manipulator(wx.Panel, action.Action):
    """ Manipulator GUI class """

    def __init__(self, parent=None, basic_func=POSITION_ROTATE):
        super(Manipulator, self).__init__(parent)
        self.id = id(self)
        self._action = 'position_rotate'
        self.model = ManipulatorModel(action=self._action)
        self.header = {'label': "MANIPULATOR",
                        'value': BASIC_FUNC_LABELS[basic_func],
                        'type': 'ComboBox',
                        'choices': list(BASIC_FUNC_LABELS.values())
        }
        self.params = []
        self.options = []
        self.rows = []
        self.bgcolor = wx.Colour(146, 153, 161)
        self.SetBackgroundColour(self.bgcolor)
        self._do_layout()
        self._do_binds()

        if basic_func == POSITION_ROTATE:
            self.position_rotate()
        elif basic_func == LOAD:
            self.load()
        elif basic_func == UNLOAD:
            self.unload()
        elif basic_func == REPOSITION:
            self.reposition()

    def _do_binds(self):
        if self.header['type'] == 'ComboBox':
            getattr(self.header_obj, 'value').Bind(wx.EVT_COMBOBOX, self._handle_combobox)

    def _handle_combobox(self, event):
        basic_func = getattr(self.header_obj, 'value').GetValue()
        if INV_BASIC_FUNC_LABELS[basic_func] == POSITION_ROTATE:
            self.position_rotate()
            self.get_values()
        elif INV_BASIC_FUNC_LABELS[basic_func] == LOAD:
            self.load()
            self.get_values()
        elif INV_BASIC_FUNC_LABELS[basic_func] == UNLOAD:
            self.unload()
            self.get_values()
        elif INV_BASIC_FUNC_LABELS[basic_func] == REPOSITION:
            self.reposition()
            self.get_values()

        event.Skip()

    def position_rotate(self):
        """ Set POSITION/ROTATE parameters and options and update layout. """
        self._action = Actions.position_rotate.key
        self.model.change_action(self._action)
        self.params = [
                        {'label': 'ROTATION POS',
                            'type': 'TextCtrl',
                            'value': DEFAULT_VALUES[self._action]['rotation_position'],
                            'mu': 'mm',
                            'key':'rotation_position'},
                        {'label': 'ROTATION ANGLE',
                            'type': 'TextCtrl',
                            'value': DEFAULT_VALUES[self._action]['rotation_angle'],
                            'mu': 'deg',
                            'key': 'rotation_angle'},
                        {'label': 'POSITION',
                            'type': 'TextCtrl',
                            'value': DEFAULT_VALUES[self._action]['position'],
                            'mu': 'mm',
                            'key': 'position'}
                      ]
        self.options = [
                        {'label': 'REDUCED_ROTATIONAL_SPEED',
                            'type': 'ComboBox',
                            'value': DEFAULT_VALUES[self._action]['reduced_rotational_speed'],
                            'mu': '',
                            'choices': ['ACTIVATED', 'DEACTIVATED'],
                            'key': 'reduced_rotational_speed'},
                        {'label': 'REDUCED_LINEAR_SPEED',
                            'type': 'ComboBox',
                            'value': DEFAULT_VALUES[self._action]['reduced_linear_speed'],
                            'mu': '',
                            'choices': ['ACTIVATED', 'DEACTIVATED'],
                            'key': 'reduced_linear_speed'},
                        {'label': 'REQUEST_NEW_BLANK',
                            'type': 'ComboBox',
                            'value': DEFAULT_VALUES[self._action]['request_new_blank'],
                            'mu': '',
                            'choices': ['ACTIVATED', 'DEACTIVATED'],
                            'key': 'request_new_blank'}
                        ]
        self.update_layout()

    def load(self):
        """ Set LOAD parameters and options and update layout. """
        self._action = Actions.load.key
        self.model.change_action(self._action)
        self.params = [
                        {'label': 'LOADING POSITION',
                            'type': 'TextCtrl',
                            'value': DEFAULT_VALUES[self._action]['loading_position'],
                            'mu': 'mm',
                            'key': 'loading_position'},
                        {'label': 'BLANK DEF(BLK)',
                            'type': 'TextCtrl',
                            'value': DEFAULT_VALUES[self._action]['blank_def'],
                            'mu': '??',
                            'key': 'blank_def'}
                      ]
        self.options = []
        self.update_layout()

    def unload(self):
        """ Set UNLOAD parameters and options and update layout. """
        self._action = Actions.unload.key
        self.model.change_action(self._action)
        self.params = [
                        {'label': 'ROTATION POS',
                            'type': 'TextCtrl',
                            'value': DEFAULT_VALUES[self._action]['rotation_position'],
                            'mu': 'mm',
                            'key': 'rotation_position'},
                        {'label': 'ROTATION ANGLE',
                            'type': 'TextCtrl',
                            'value': DEFAULT_VALUES[self._action]['rotation_angle'],
                            'mu': 'deg',
                            'key': 'rotation_angle'},
                        {'label': 'UNLOAD POSITION',
                            'type': 'TextCtrl',
                            'value': DEFAULT_VALUES[self._action]['unload_position'],
                            'mu': 'mm',
                            'key': 'unload_position'}
                      ]
        self.options = [{'label': 'UNLOADING_BELT',
                            'type': 'ComboBox',
                            'value': DEFAULT_VALUES[self._action]['unloading_belt'],
                            'mu': '',
                            'choices': ['ACTIVATED', 'DEACTIVATED'],
                            'key': 'unloading_belt'}]
        self.update_layout()

    def reposition(self):
        """ Set REPOSITION parameters and options and update layout. """
        self._action = Actions.reposition.key
        self.model.change_action(self._action)
        self.params = [
                        {'label': 'PULL BACK',
                            'type': 'TextCtrl',
                            'value': DEFAULT_VALUES[self._action]['pull_back'],
                            'mu': 'mm',
                            'key': 'pull_back'},
                        {'label': 'UPPER-BEAM DOWN',
                            'type': 'TextCtrl',
                            'value': DEFAULT_VALUES[self._action]['upper_beam_down'],
                            'mu': 'mm',
                            'key': 'upper_beam_down'},
                        {'label': 'OFFSET',
                            'type': 'TextCtrl',
                            'value': DEFAULT_VALUES[self._action]['offset'],
                            'mu': 'mm',
                            'key': 'offset'},
                        {'label': 'UPPER-BEAM UP',
                            'type': 'TextCtrl',
                            'value': DEFAULT_VALUES[self._action]['upper_beam_up'],
                            'mu': 'mm',
                            'key': 'upper_beam_up'}
                      ]
        self.options = [{'label': 'HALF_UP',
                            'type': 'ComboBox',
                            'value': DEFAULT_VALUES[self._action]['half_up'],
                            'mu': '',
                            'choices': ['ACTIVATED', 'DEACTIVATED'],
                            'key': 'half_up'}]
        self.update_layout()

    def update_layout(self):
        """ Update the layout. """
        for row in self.rows:
            self.sizer.Detach(row)
        self.rows = []
        for param in self.params:
            row = Row(self, param)
            self.rows.append(row)
            self.sizer.Add(row, 0, wx.EXPAND | wx.TOP, 1)
        for option in self.options:
            row = Row(self, option)
            self.rows.append(row)
            self.sizer.Add(row, 0, wx.EXPAND | wx.TOP, 1)
        self.GetParent().Layout()

    def __str__(self):
        return self.__class__.__name__

    def _do_layout(self):
        self.sizer = wx.BoxSizer(wx.VERTICAL)

        self.header_obj = Header(self, self.header)
        self.sizer.Add(self.header_obj, 0, wx.EXPAND)

        for param in self.params:
            row = Row(self, param)
            self.rows.append(row)
            self.sizer.Add(row, 0, wx.EXPAND | wx.TOP, 1)
            #self.sizer.Add(wx.StaticLine(self, wx.LI_HORIZONTAL), 0, wx.EXPAND | wx.BOTTOM, 1)
        for option in self.options:
            row = Row(self, option)
            self.rows.append(row)
            self.sizer.Add(row, 0, wx.EXPAND | wx.TOP, 1)

        #self.sizer.Add((0, 0), 1, wx.EXPAND)
        self.SetSizer(self.sizer)

    def get_values(self):
        for row in self.rows:
            print(row)

    # TODO
    def destroy(self):
        """ Destroy children & self. """
        # self.remove_children()
        # self.GetParent().remove_action(self)
        self.Destroy()
#-----------------------------------------------------------------------------


if __name__ == '__main__':
    app = wx.App()
    frame = wx.Frame(None)
    panel = wx.Panel(frame)
    sizer = wx.BoxSizer(wx.VERTICAL)
    manipulator = Manipulator(panel)
    sizer.Add(manipulator, 0, wx.EXPAND)
    panel.SetSizer(sizer)
    frame.Show()
    manipulator.get_values()
    app.MainLoop()