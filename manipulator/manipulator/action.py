# -*- coding: utf-8 -*-


class Action:

    def __init__(self):
        pass

    def print_params(self):
        if self.params:
            for param in self.params:
                print(param)

    def has_children(self):
        if self.children:
            return True
        return False

    def print_children(self):
        if self.children:
            for child in self.children:
                for param in child.params:
                    print(param)
