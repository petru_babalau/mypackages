.. manipulator documentation master file, created by
   sphinx-quickstart on Wed Aug  9 20:54:45 2017.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

.. toctree::
   :maxdepth: 2
   :caption: Contents:


manipulator.Manipulator
=======================================
*Manipulator* package contains the main manipulator basic movemenet functions.::

Manipulator basic functions:
 

- ``POSITION/ROTATE``: position rotate
- ``LOAD``: load
- ``UNLOAD``: unload
- ``REPOSITION``: reposition


.. _Button-styles:

Class Hierarchy
================================
 


Known Subclasses
================================



Methods Summary
================================

================================================================================ ================================================================================
:meth:`~manipulator.Manipulator.__init__`                                        Default constructor.
:meth:`~wx.Button.Create`                                                        Button creation function for two-step creation.
:meth:`~wx.Button.GetAuthNeeded`                                                 Returns ``True`` if an authentication needed symbol is displayed on the button.
:meth:`~wx.Button.GetDefaultSize`                                                Returns the default size for the buttons.
:meth:`~wx.Button.GetLabel`                                                      Returns the string label for the button.
:meth:`~wx.Button.SetAuthNeeded`                                                 Sets whether an authentication needed symbol should be displayed on the button.
:meth:`~wx.Button.SetDefault`                                                    This sets the button to be the default item in its top-level window (e.g.
:meth:`~wx.Button.SetLabel`                                                      Sets the string label for the button.
================================================================================ ================================================================================




|


Properties Summary
================================

================================================================================ ================================================================================
:attr:`~wx.Button.AuthNeeded`                                                    See :meth:`~wx.Button.GetAuthNeeded` and :meth:`~wx.Button.SetAuthNeeded`
:attr:`~wx.Button.Label`                                                         See :meth:`~wx.Button.GetLabel` and :meth:`~wx.Button.SetLabel`
================================================================================ ================================================================================


|



Class API
================================

.. class:: manipulator.Manipulator()


   **Possible constructors**::

       Button()
       
       Button(parent, id=ID_ANY, label="", pos=DefaultPosition,
              size=DefaultSize, style=0, validator=DefaultValidator,
              name=ButtonNameStr)
       
   
   A button is a control that contains a text string, and is one of the
   most common elements of a GUI.



   .. method:: __init__(self, *args, **kw)



      |overload| Overloaded Implementations:

      **~~~**

      
      **__init__** `(self)`
      
      Default constructor.                   
      
      
      
      
      **~~~**

      
      **__init__** `(self, parent, id=ID_ANY, label="", pos=DefaultPosition, size=DefaultSize, style=0, validator=DefaultValidator, name=ButtonNameStr)`
      
      Constructor, creating and showing a button.                  
      
      The preferred way to create standard buttons is to use default value of `label`. If no label is supplied and `id`  is one of standard IDs from :ref:`this list <stock items>`, a standard label will be used. In other words, if you use a predefined  ``ID_XXX``   constant, just omit the label completely rather than specifying it. In particular, help buttons (the ones with  `id`  of  ``ID_HELP`` ) under Mac OS X can't display any label at all and while  :ref:`wx.Button`  will detect if the standard "Help" label is used and ignore it, using any other label will prevent the button from correctly appearing as a help button and so should be avoided. 
      
      In addition to that, the button will be decorated with stock icons under GTK+ 2. 
      
      
      
      
      :param `parent`: Parent window. Must not be ``None``.   
      :type `parent`: wx.Window
      :param `id`: Button identifier. A value of  ``ID_ANY``   indicates a default value.    
      :type `id`: wx.WindowID
      :param `label`: Text to be displayed on the button.   
      :type `label`: string
      :param `pos`: Button position.   
      :type `pos`: wx.Point
      :param `size`: Button size. If the default size is specified then the button is sized appropriately for the text.   
      :type `size`: wx.Size
      :param `style`: Window style. See :ref:`wx.Button`  class description.   
      :type `style`: long
      :param `validator`: Window validator.   
      :type `validator`: wx.Validator
      :param `name`: Window name.  
      :type `name`: string
      
      
      
      
      
      
      
      
      
      
      
      
      
      
      
      
      
      
                        
      
      
      
      .. seealso:: :meth:`Create` , :ref:`wx.Validator`    
      
      
      
      
      
      
      
      **~~~**






   .. method:: Create(self, parent, id=ID_ANY, label="", pos=DefaultPosition, size=DefaultSize, style=0, validator=DefaultValidator, name=ButtonNameStr)

      Button creation function for two-step creation.                  

      For more details, see :ref:`wx.Button`.                  


      :param `parent`: 
      :type `parent`: wx.Window
      :param `id`: 
      :type `id`: wx.WindowID
      :param `label`: 
      :type `label`: string
      :param `pos`: 
      :type `pos`: wx.Point
      :param `size`: 
      :type `size`: wx.Size
      :param `style`: 
      :type `style`: long
      :param `validator`: 
      :type `validator`: wx.Validator
      :param `name`: 
      :type `name`: string




      :rtype: `bool`








   .. method:: GetAuthNeeded(self)

      Returns ``True`` if an authentication needed symbol is displayed on the button.                  

                

      :rtype: `bool`







      .. versionadded:: 2.9.1 
     







      .. note:: 

         This method always returns ``False`` if the platform is not Windows Vista or newer.  







      .. seealso:: :meth:`SetAuthNeeded`   








   .. staticmethod:: GetDefaultSize()

      Returns the default size for the buttons.                  

      It is advised to make all the dialog buttons of the same size and this function allows to retrieve the (platform and current font dependent size) which should be the best suited for this.                  

      :rtype: :ref:`wx.Size`








   .. method:: GetLabel(self)

      Returns the string label for the button.                  

                

      :rtype: `string`







      .. seealso:: :meth:`SetLabel`     








   .. method:: SetAuthNeeded(self, needed=True)

      Sets whether an authentication needed symbol should be displayed on the button.                  

                


      :param `needed`: 
      :type `needed`: bool






      .. versionadded:: 2.9.1 
     







      .. note:: 

         This method doesn't do anything if the platform is not Windows Vista or newer.  







      .. seealso:: :meth:`GetAuthNeeded`   








   .. method:: SetDefault(self)

      This sets the button to be the default item in its top-level window (e.g.                  

      the panel or the dialog box containing it). 

      As normal, pressing return causes the default button to be depressed when the return key is pressed. 

      See also :meth:`wx.Window.SetFocus`   which sets the keyboard focus for windows and text panel items, and :meth:`wx.TopLevelWindow.SetDefaultItem` . 

                

      :rtype: :ref:`wx.Window`







      :returns: 

         the old default item (possibly ``None``)   







      .. note:: 

         Under Windows, only dialog box buttons respond to this function.  








   .. method:: SetLabel(self, label)

      Sets the string label for the button.                  




      :param `label`: The label to set.   
      :type `label`: string




                  





   .. attribute:: AuthNeeded

      See :meth:`~wx.Button.GetAuthNeeded` and :meth:`~wx.Button.SetAuthNeeded`


   .. attribute:: Label

      See :meth:`~wx.Button.GetLabel` and :meth:`~wx.Button.SetLabel`

=====  =====  =======
A      B      A and B
=====  =====  =======
False  False  False
True   False  False
False  True   False
True   True   True
=====  =====  =======


Classes
==================
.. automodule:: manipulator
   :members:
